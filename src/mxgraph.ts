import mxgraphFactory from "mxgraph";

const factory = mxgraphFactory({
  mxImageBasePath: "assets/mxgraph/images",
  mxBasePath: "assets/mxgraph"
});

const {
  mxClient,
  mxEvent,
  mxGraph,
  mxGraphView,
  mxPoint,
  mxRectangle,
  mxRectangleShape,
  mxUtils,
  mxDragSource,
  mxPopupMenuHandler,
  mxPopupMenu,
  mxKeyHandler,
  mxRubberband,
  mxConstants,
  mxMouseEvent,
  mxCell,
  mxGeometry,
  mxGraphModel,
  mxCompactTreeLayout,
  mxUndoManager,
  mxGraphHandler,
  mxVertexHandler,
  mxPanningHandler,
  mxCellHighlight
} = factory;

export { mxClient };
export { mxEvent };
export { mxGraph };
export { mxGraphView };
export { mxPoint };
export { mxRectangle };
export { mxRectangleShape };
export { mxUtils };
export { mxDragSource };
export { mxPopupMenuHandler };
export { mxPopupMenu };
export { mxKeyHandler };
export { mxRubberband };
export { mxConstants };
export { mxMouseEvent };
export { mxCell };
export { mxGeometry };
export { mxGraphModel };
export { mxCompactTreeLayout };
export { mxUndoManager };
export { mxGraphHandler };
export { mxVertexHandler };
export { mxPanningHandler };
export { mxCellHighlight };

