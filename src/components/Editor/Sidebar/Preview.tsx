import React, { useState } from "react";
import { Button, Classes, Overlay, Tab } from "@blueprintjs/core";

import { ControlInfoDto } from "../controls/сontrolInfoDto";
import controls from "../controls";
import ControlType from "../controls/controlType";
import Dashboard from "../controls/Dashboard/Dashboard";
import { uuidv4 } from "../../../services/controlService";
import Tabs from "../controls/Tabs/Tabs";
import { useControlTreeState } from "../Providers/ControlTreeProvider";

// файл состоит польностью из костылей для реализации предпросмотра
// dev purpose only

const FakePanel: React.FC<{ uuid: string }> = ({ uuid, children }) => {
  return (
    <Tab
      key={uuid}
      id={uuid}
      title={uuid.split("-")[0]}
      panel={<div>{children}</div>}
    />
  );
};

function withTabsOption(Component: typeof Tabs, uuid: string) {
  return ({ ...props }) => {
    const [tab, activeTab] = useState(uuid);
    return <Component {...props} selectedTabId={tab} onChange={activeTab} />;
  };
}

function withPanelUuid(Component: typeof FakePanel, uuid: string) {
  return ({ ...props }) => <Component {...props} uuid={uuid} />;
}

function enhanceTabsOptions(
  options: { tabs: string[] },
  panels: ControlInfoDto[]
) {
  options.tabs = [];
  panels.forEach(panel => {
    panel.guid = uuidv4();
    options.tabs.push(panel.guid);
  });
}

function render(controlInfo: ControlInfoDto) {
  let Control: React.FC | null = controls[controlInfo.type];
  if (!Control) {
    switch (controlInfo.type) {
      case ControlType.Dashboard: {
        Control = Dashboard;
        break;
      }
      case ControlType.Panel: {
        Control = withPanelUuid(FakePanel, controlInfo.guid);
        const uuid = controlInfo.guid;
        return (
          <Tab
            key={uuid}
            id={uuid}
            title={uuid.split("-")[0]}
            panel={
              <div>{controlInfo.children.map(child => render(child))}</div>
            }
          />
        );
      }
      default:
        break;
    }
  }

  if (controlInfo.type === ControlType.Tabs) {
    enhanceTabsOptions(controlInfo.options[1] as any, controlInfo.children);
    Control = withTabsOption(
      controls[controlInfo.type],
      controlInfo.children[0].guid
    );
  }

  const { width, height, x, y } = controlInfo.geometry;
  return (
    <div
      key={uuidv4()}
      style={{
        position: [ControlType.Tabs, ControlType.Dashboard].includes(
          controlInfo.type
        )
          ? "relative"
          : "absolute",
        display: "inline-block",
        width: `${width}px`,
        height: `${height}px`,
        top: `${y}px`,
        left: `${x}px`
      }}
    >
      {Control && (
        <Control>{controlInfo.children.map(child => render(child))}</Control>
      )}
    </div>
  );
}

const PreviewContent: React.FC = () => {
  const { controlTree } = useControlTreeState();
  if (controlTree.length === 0) return null;
  const _fake_controlTree = controlTree[0].toDto();
  if (!_fake_controlTree) return null;
  return <>{render(_fake_controlTree)}</>;
};

const Preview: React.FC = () => {
  const [isOpen, setOpen] = useState(false);

  return (
    <>
      <Button
        style={{
          position: "absolute",
          bottom: 0,
          left: "40px",
          margin: "10px"
        }}
        icon="list-detail-view"
        large
        minimal
        onClick={() => setOpen(isOpen => !isOpen)}
      />
      <Overlay
        transitionDuration={0}
        isOpen={isOpen}
        onClose={() => setOpen(false)}
        className={Classes.OVERLAY_SCROLL_CONTAINER}
      >
        <div className={Classes.CARD} style={{ margin: "50px" }}>
          <PreviewContent />
        </div>
      </Overlay>
    </>
  );
};

export default Preview;
