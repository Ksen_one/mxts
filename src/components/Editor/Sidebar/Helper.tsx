import React, { useState } from "react";
import { Button, Dialog, Classes, Menu } from "@blueprintjs/core";

const Helper: React.FC = () => {
  const [isOpen, setOpen] = useState(false);
  return (
    <>
      <Button
        style={{ position: "absolute", bottom: 0, margin: "10px" }}
        icon="info-sign"
        large
        minimal
        onClick={() => setOpen(isOpen => !isOpen)}
      />
      <Dialog
        isOpen={isOpen}
        onClose={() => setOpen(false)}
        icon="info-sign"
        title="Editor help"
      >
        <div className={Classes.DIALOG_BODY}>
          <Menu style={{ pointerEvents: "none" }}>
            <Menu.Divider title="Zoom" />
            <Menu.Item text="Movements" label="R Mouse" icon="move" />
            <Menu.Item text="Zoom in" label="S + Wheele up" icon="zoom-in" />
            <Menu.Item
              text="Zoom out"
              label="S + Wheele down"
              icon="zoom-out"
            />
            <Menu.Divider title="Work with selection" />
            <Menu.Item text="Select" label="L Mouse" icon="select" />
            <Menu.Item text="Delete" label="Del" icon="trash" />
          </Menu>
        </div>
      </Dialog>
    </>
  );
};

export default Helper;
