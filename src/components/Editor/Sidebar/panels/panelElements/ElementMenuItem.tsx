import React, { useRef } from "react";
import { Classes } from "@blueprintjs/core";

import ControlType from "../../../controls/controlType";
import { ElementSize } from "../../../controls/controlMetadata";
import useDraggable from "./useDraggable.hook";

type ElementMenuItemProps = {
  name: string;
  size: ElementSize;
  type: ControlType;
  icon?: string;
};

const ElementMenuItem: React.FC<ElementMenuItemProps> = props => {
  const menuItemRef = useRef<HTMLLIElement>(null);
  const { name, icon, type, size } = props;

  useDraggable(menuItemRef, type, size);

  const buttonClass = `${Classes.MENU_ITEM}${icon ? ` bp3-icon-${icon}` : ""}`;
  return (
    <li ref={menuItemRef}>
      <button type="button" className={buttonClass}>
        {name}
      </button>
    </li>
  );
};

export default ElementMenuItem;
