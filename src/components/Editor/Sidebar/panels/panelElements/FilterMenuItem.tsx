import React from "react";
import { InputGroup, Button, Classes } from "@blueprintjs/core";

const FilterMenuItem: React.FC<{
  filter: string;
  setFilter: React.Dispatch<React.SetStateAction<string>>;
}> = ({ filter, setFilter }) => {
  return (
    <li className={Classes.MENU_HEADER}>
      <InputGroup
        round={true}
        fill={true}
        leftIcon="search"
        placeholder="Найти..."
        value={filter}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setFilter(event.target.value)
        }
        rightElement={
          filter ? (
            <Button icon="cross" minimal={true} onClick={() => setFilter("")} />
          ) : (
            undefined
          )
        }
      />
    </li>
  );
};

export default FilterMenuItem;
