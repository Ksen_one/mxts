import { useEffect } from "react";

import { mxUtils, mxEvent } from "../../../../../mxgraph";
import { useGraph } from "../../../Providers/EditorProviders";
import { useControlTreeActions } from "../../../Providers/ControlTreeProvider";
import ControlType from "../../../controls/controlType";
import { ElementSize } from "../../../controls/controlMetadata";
import ControlBuilder from "../../../controls/controlBuilder";
import { EditorControlInfo } from "../../../controls/controlInfo";

function getPreviewElement({ width, height }: ElementSize) {
  var dragElt = document.createElement("div");
  dragElt.style.border = "dashed black 1px";
  dragElt.style.width = `${width}px`;
  dragElt.style.height = `${height}px`;
  return dragElt;
}

const useDraggable = (
  ref: React.RefObject<HTMLLIElement>,
  type: ControlType,
  initialSize: ElementSize
) => {
  const { state: graph } = useGraph();
  const { addControl } = useControlTreeActions();

  useEffect(() => {
    const draggableElement = ref.current;

    if (!draggableElement || !graph) return;

    const dropHandler = (
      graph: mxGraph,
      evt: any,
      target: mxCell | null,
      x: number,
      y: number
    ) => {
      if (
        !target ||
        !target.geometry ||
        !(target.value instanceof EditorControlInfo)
      )
        return;

      const geometry = {
        x: x - target.geometry.x,
        y: y - target.geometry.y,
        ...initialSize
      };

      const control = ControlBuilder.type(type)
        .createControl(target.value)
        .createCell(graph, geometry);

      if (control.type === ControlType.Tabs) {
        for (let i = 0; i < 3; i++) {
          ControlBuilder.type(ControlType.Panel)
            .createControl(control)
            .createCell(graph);
        }
      }

      addControl(control);
    };

    const getTarget = (graph: mxGraph, x: number, y: number) => {
      const cell = graph.getCellAt(x, y);
      return graph.isValidDropTarget(cell) ? cell : null;
    };

    const draggable = mxUtils.makeDraggable(
      draggableElement,
      () => graph,
      dropHandler,
      getPreviewElement(initialSize),
      null,
      null,
      false,
      true,
      true,
      getTarget
    );

    draggable.guidesEnabled = true;

    return () => {
      draggable.reset();
      mxEvent.removeAllListeners(draggableElement);
    };
  }, [ref, graph, initialSize, addControl, type]);
};

export default useDraggable;
