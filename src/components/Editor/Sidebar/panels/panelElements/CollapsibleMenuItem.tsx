import React, { useState } from "react";
import {
  MenuItem,
  Collapse as BlueprintCollapse,
  Classes
} from "@blueprintjs/core";

const CollapsibleMenuItem: React.FC<{
  name: string;
  openedByDefault?: boolean;
}> = ({ name, openedByDefault = true, children }) => {
  const [isOpen, setIsOpen] = useState(openedByDefault);

  return (
    <>
      <MenuItem
        text={name}
        icon={isOpen ? "chevron-up" : "chevron-right"}
        onClick={() => setIsOpen(isOpen => !isOpen)}
        textClassName={Classes.TEXT_MUTED}
      />
      <BlueprintCollapse
        isOpen={isOpen}
        keepChildrenMounted={true}
        transitionDuration={0}
      >
        <div style={{ paddingLeft: "20px" }}>{children}</div>
      </BlueprintCollapse>
    </>
  );
};

export default CollapsibleMenuItem;
