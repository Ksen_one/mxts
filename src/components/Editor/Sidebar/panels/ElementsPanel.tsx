import React, { useState } from "react";
import { MenuItem, Menu, MenuDivider } from "@blueprintjs/core";

import { useGraph } from "../../Providers/EditorProviders";
import controls from "../../controls";
import ElementMenuItem from "./panelElements/ElementMenuItem";
import CollapsibleMenuItem from "./panelElements/CollapsibleMenuItem";
import FilterMenuItem from "./panelElements/FilterMenuItem";

const menuItems: { controlName: string; menuItem: JSX.Element }[] = [];
for (const [type, control] of Object.entries(controls)) {
  if (!control) continue;
  const { controlName, icon, size } = control;
  menuItems.push({
    controlName,
    menuItem: (
      <ElementMenuItem
        key={controlName}
        name={controlName}
        size={size}
        type={Number(type)} // object.entries returns string keys
        icon={icon}
      />
    )
  });
}

const ElementsPanel = () => {
  const { state: graph } = useGraph();
  const [filter, setFilter] = useState("");

  if (!graph) return null;

  const filtredMenuItems =
    filter === ""
      ? menuItems
      : menuItems.filter(({ controlName }) =>
          controlName.match(new RegExp(`${filter}`, "i"))
        );

  return (
    <Menu large={true} style={{ background: "none" }}>
      <FilterMenuItem filter={filter} setFilter={setFilter} />
      <CollapsibleMenuItem name={"Шаблоны"} openedByDefault={false}>
        <MenuItem text="not implemented" icon="warning-sign" label="⌘X" disabled />
      </CollapsibleMenuItem>
      <MenuDivider />
      <CollapsibleMenuItem name={"Компоненты"} openedByDefault={false}>
        <MenuItem text="not implemented" icon="warning-sign" label="⌘Y" disabled />
      </CollapsibleMenuItem>
      <MenuDivider />
      <CollapsibleMenuItem name={"Элементы"}>
        {filtredMenuItems.map(({ menuItem }) => menuItem)}
      </CollapsibleMenuItem>
    </Menu>
  );
};

export default ElementsPanel;
