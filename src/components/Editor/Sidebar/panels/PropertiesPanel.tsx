import React from "react";
import { useSelection } from "../../Providers/SelectionProvider";
import { EditorControlInfo } from "../../controls/controlInfo";
import { useControlTreeActions } from "../../Providers/ControlTreeProvider";

const PropertiesPanel = () => {
  const selection = useSelection();

  if (selection.length !== 1) return null;
  const control =
    selection[0].value instanceof EditorControlInfo ? selection[0].value : null;
  if (!control) return null;
  return (
    <div>
      {control.guid}
      <ButtonConfigurator control={control} />
    </div>
  );
};

const ButtonConfigurator: React.FC<{ control: EditorControlInfo }> = ({
  control
}) => {
  console.count("bconf");

  const { updateControl } = useControlTreeActions();
  // const { controlTree } = useEditorState();
  // const ctrl = controlTree.find(cnrtrl => cnrtrl === control);
  // if (!ctrl) return null;
  // const value = (ctrl.options[0] as any).text;
  return (
    <div>
      <input
        type="text"
        value={(control.options[0] as any).text}
        onChange={({ target: { value } }) => {
          // (ctrl.options[0] as any).text = value;
          (control.options[0] as any).text = value;
          updateControl(control);
        }}
      />
    </div>
  );
};

export default PropertiesPanel;
