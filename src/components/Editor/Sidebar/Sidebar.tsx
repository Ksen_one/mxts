import React from "react";
import { Navbar, Tab, Tabs } from "@blueprintjs/core";

import styles from "./Sidebar.module.scss";
import ElementsPanel from "./panels/ElementsPanel";
import Helper from "./Helper";
import Preview from "./Preview";

const menuPanels = {
  Элементы: <ElementsPanel />,
  Стили: <div />,
  Свойства: <div /> //<PropertiesPanel />
} as const;

type MenuPanelsKeys = keyof typeof menuPanels;

const menuPanelsKeys = Object.keys(menuPanels) as [MenuPanelsKeys];

const Sidebar: React.FC = () => {
  const [panelId, setPanelId] = React.useState(menuPanelsKeys[0]);
  const panel = menuPanels[panelId];

  return (
    <div className={styles.sidebar}>
      <Navbar className={styles.sidebarNav}>
        <Tabs
          className={styles.sidebarTabs}
          id="editorSidebar"
          animate={true}
          large={true}
          onChange={(key: MenuPanelsKeys) => setPanelId(key)}
          selectedTabId={panelId}
        >
          {menuPanelsKeys.map(key => (
            <Tab
              className={styles.sidebarPanel}
              key={key}
              id={key}
              title={key}
            />
          ))}
        </Tabs>
      </Navbar>

      {panel}

      <Helper />
      <Preview />
    </div>
  );
};

export default Sidebar;
