import React from "react";
import { Spinner } from "@blueprintjs/core";

import styles from './Loader.module.css';
import { useEditorState } from "../Providers/EditorStateProvider";

const Loader: React.FC = () => {
  const { loading } = useEditorState();

  if (loading) {
    return (
      <div className={styles.loader}>
        <Spinner />
      </div>
    );
  }
  return null;
};

export default Loader;
