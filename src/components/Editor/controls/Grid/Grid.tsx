import React from "react";
import { Grid as KendoGrid, GridColumn } from "@progress/kendo-react-grid";
import "@progress/kendo-theme-default/dist/all.css";

import products from "./products.json";
import { ControlMetadata } from "../controlMetadata";

const Grid: React.FC & ControlMetadata = () => {
  return (
    <KendoGrid data={products} style={{ width: "100%", height: "100%" }}>
      <GridColumn field="ProductName" />
      <GridColumn field="UnitPrice" />
      <GridColumn field="UnitsInStock" />
      <GridColumn field="Discontinued" />
    </KendoGrid>
  );
};

Grid.controlName = "Таблица" as const;
Grid.icon = "th" as const;
Grid.size = { width: 300, height: 200 };

export default Grid;
