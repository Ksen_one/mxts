import React, { useMemo } from "react";
import { InputGroup, FormGroup } from "@blueprintjs/core";
import { ControlMetadata } from "../controlMetadata";

type InputProps = {
  required?: boolean;
  helperText?: string;
  label?: string;
  placeholder?: string;
};

const Input: React.FC<InputProps> & ControlMetadata = ({
  required = false,
  helperText = "Helper text with details...",
  label = "Label",
  placeholder = "Placeholder text"
}) => {
  const id = useMemo(() => `${Date.now() + Math.random() * 1000}`, []);

  return (
    <FormGroup
      helperText={helperText}
      label={label}
      labelFor={id}
      labelInfo={required ? "(required)" : undefined}
    >
      <InputGroup id={id} placeholder={placeholder} />
    </FormGroup>
  );
};

Input.controlName = "Текст" as const;
Input.icon = "text-highlight" as const;
Input.size = { width: 200, height: 80 };

export default Input;
