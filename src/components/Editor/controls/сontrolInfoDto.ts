import ControlType from "./controlType";
import { ElementSize, ElementPosition } from "./controlMetadata";
import { ControlOptions } from "./controlInfo";

export type ControlInfoDto<T extends ControlType = ControlType> = {
  guid: string;
  type: T;
  geometry: ElementSize & ElementPosition;
  options: ControlOptions<T>;
  children: ControlInfoDto<ControlType>[];
};
