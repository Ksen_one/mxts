import ControlOptionType from "./controlOptionType";
import { PanelControlInfo } from "./controlInfo";

export abstract class ControlOption {}

class ButtonControlOption extends ControlOption {
  public text?: string;
}

class TabsControlOption extends ControlOption {
  public tabs: PanelControlInfo[] = [];

  public toJSON() {
    return { tabs: this.tabs.length };
  }
}

class TestControlOption extends ControlOption {
  public test?: string;
}

export const controlOptionByType = {
  [ControlOptionType.Button]: ButtonControlOption,
  [ControlOptionType.Tabs]: TabsControlOption,
  [ControlOptionType.Test]: TestControlOption
} as const;
