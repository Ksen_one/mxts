import { mxGeometry, mxCell, mxEvent } from "../../../mxgraph";

import {
  isMxChildChange,
  isMxGeometryChange
} from "../../../services/graphService";
import { uuidv4 } from "../../../services/controlService";
import ControlType from "./controlType";
import { ElementSize, ElementPosition } from "./controlMetadata";
import ControlOptionType from "./controlOptionType";
import { ControlInfoDto } from "./сontrolInfoDto";
import { controlOptionByType } from "./controlOption";

const availableOptionsForControlType = {
  [ControlType.Dashboard]: [ControlOptionType.Test],
  [ControlType.Input]: [ControlOptionType.Test],
  [ControlType.Button]: [ControlOptionType.Button, ControlOptionType.Test],
  [ControlType.Label]: [],
  [ControlType.Grid]: [],
  [ControlType.Tabs]: [ControlOptionType.Tabs],
  [ControlType.Panel]: []
} as const;

type ControlOptionByType<T extends ControlOptionType> = InstanceType<
  typeof controlOptionByType[T]
>;
type Prop<T> = {
  [K in keyof T]: T[K] extends Function ? never : K;
}[keyof T];

type AvailableOptions<
  T extends ControlType
> = typeof availableOptionsForControlType[T];

export type ControlOptions<T extends ControlType> = {
  [OptionsType in AvailableOptions<T>[number]]?: {
    [K in Prop<ControlOptionByType<OptionsType>>]: ControlOptionByType<
      OptionsType
    >[K];
  };
};

export default class ControlInfo<T extends ControlType = ControlType> {
  public readonly guid: string;
  public readonly type: T;
  public readonly options: ControlOptions<T>;
  public readonly children: ControlInfo[] = [];
  public parent: ControlInfo | null;

  public readonly clientGuid: string;
  public constructor(type: T, parent: ControlInfo | null = null) {
    this.type = type;

    this.parent = parent;
    if (this.parent) {
      this.parent.addChildren([this]);
    }

    this.guid = "0";
    this.clientGuid = uuidv4();

    this.options = this.initDefaultOptions();
  }

  private initDefaultOptions(): ControlOptions<T> {
    const availableOptionTypes = availableOptionsForControlType[this.type];
    const result = {} as any;
    if (availableOptionTypes.length > 0) {
      for (const optionType of availableOptionTypes) {
        result[optionType] = new controlOptionByType[optionType]();
      }
    }
    return result;
  }

  public setOptions(options: ControlOptions<T>) {
    const availableOptionTypes = availableOptionsForControlType[this.type];
    for (let i = 0; i < availableOptionTypes.length; i++) {
      const optionType: AvailableOptions<T>[number] = availableOptionTypes[i];
      Object.assign(this.options[optionType], options[optionType]);
    }
    return this;
  }

  public addChildren(children: ControlInfo[]) {
    this.children.push(...children);
  }

  public removeChild(child: ControlInfo) {
    const index = this.children.indexOf(child);
    if (index >= 0) this.children.splice(index, 1);
  }
}

export class EditorControlInfo<
  T extends ControlType = ControlType
> extends ControlInfo<T> {
  public parent: EditorControlInfo | null;
  public readonly children: EditorControlInfo[] = [];
  public readonly node: HTMLDivElement;

  public constructor(
    type: T,
    parent: EditorControlInfo | null = null,
    node?: HTMLDivElement
  ) {
    super(type, parent);

    this.parent = parent;

    if (node) {
      this.node = node;
    } else {
      this.node = document.createElement("div");
      this.node.style.pointerEvents = "none";
    }
  }

  public createCell(
    graph: mxGraph,
    { x, y, width, height }: ElementSize & ElementPosition
  ) {
    const parentCell =
      (this.parent && this.parent.getCell()) || graph.getDefaultParent();
    const cell = graph.insertVertex(
      parentCell,
      this.guid,
      this,
      x,
      y,
      width,
      height
    );

    this.linkCell(cell);

    return this;
  }

  protected cell: mxCell<EditorControlInfo<T>> | null = null;
  public linkCell(cell: mxCell<EditorControlInfo<T>>) {
    this.cell = cell;
  }
  public getCell() {
    return this.cell;
  }

  public toDto(): ControlInfoDto<T> {
    const cell = this.getCell();
    if (!cell || !cell.geometry) throw new Error();
    const { width, height, x, y } = cell.geometry;
    const geometry = { height, width, x: Math.ceil(x), y: Math.ceil(y) };

    const dto = { geometry } as any;
    for (const [key, value] of Object.entries(this)) {
      if (this.transientProps().includes(key)) continue;
      if (key === "children") {
        dto.children = this.children.map(child => child.toDto());
      } else {
        dto[key] = value;
      }
    }
    return dto;
  }

  private transientProps() {
    return ["node", "parent", "cell", "clientGuid"];
  }
}

export class DashboardControlInfo extends EditorControlInfo<
  ControlType.Dashboard
> {
  public constructor() {
    super(ControlType.Dashboard, null);
  }

  public createCell = (graph: mxGraph) => {
    const { width, height } = graph.pageFormat;
    const cell = graph.insertVertex(
      graph.getDefaultParent(),
      this.guid,
      this,
      0,
      0,
      width,
      height,
      "dashboard"
    );
    this.linkCell(cell);
    return this;
  };
}

export class TabsControlInfo extends EditorControlInfo<ControlType.Tabs> {
  public readonly children: PanelControlInfo[] = [];
  public constructor(parent?: EditorControlInfo) {
    super(ControlType.Tabs, parent, document.createElement("div"));
  }

  public createCell = (
    graph: mxGraph,
    { x, y, width, height }: ElementSize & ElementPosition
  ) => {
    const cellGeometry = new mxGeometry(x, y, width, height);
    const cell = new mxCell(this, cellGeometry);
    this.linkCell(cell);

    // Перемещаем и удаляем все вкладки табов вместе
    const handleChildsUpdate = (sender: any, evt: any) => {
      const changes = evt.getProperty("edit").changes;

      for (let i = 0; i < changes.length; i++) {
        const change = changes[i];
        if (isMxChildChange(change)) {
          const cells = this.children.map(child => child.getCell()!);
          if (cells.some(childCell => childCell === change.child)) {
            const otherCells = cells.filter(c => c !== change.child);
            if (change.parent) {
              for (const otherChildCell of otherCells) {
                change.parent.insert(otherChildCell);
              }
            } else {
              graph.removeCells(otherCells);
              graph.getModel().removeListener(handleChildsUpdate);
            }
            break;
          }
        } else if (isMxGeometryChange(change)) {
          const cells = this.children.map(child => child.getCell()!);
          if (cells.some(childCell => childCell === change.cell)) {
            const otherCells = cells.filter(c => c !== change.cell);
            otherCells.forEach(otherChildCell =>
              otherChildCell.setGeometry(change.geometry)
            );
            cell.setGeometry(change.geometry);
          }
        }
      }
    };

    graph.getModel().addListener(mxEvent.CHANGE, handleChildsUpdate);

    return this;
  };
}

export class PanelControlInfo extends EditorControlInfo<ControlType.Panel> {
  public readonly parent: TabsControlInfo;

  constructor(parent: TabsControlInfo) {
    super(ControlType.Panel, parent, parent.node);
    this.parent = parent;
    const tabsOptions = this.parent.options[ControlOptionType.Tabs];
    if (tabsOptions) {
      tabsOptions.tabs.push(this);
    }
  }

  public createCell = (graph: mxGraph) => {
    const tabsRootGeometry = this.parent?.getCell()?.geometry;
    if (!tabsRootGeometry) throw new Error("Can't resolve tab's root geometry");
    const cell = new mxCell(this, tabsRootGeometry, "tab");
    cell.vertex = true;
    cell.visible = false;

    this.linkCell(cell);

    graph.addCells([cell], this.parent.parent?.getCell());

    return this;
  };
}
