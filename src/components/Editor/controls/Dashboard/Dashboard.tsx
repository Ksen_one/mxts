import React from "react";

const Dashboard: React.FC = ({ children }) => {
  return (
    <div
      style={{
        backgroundColor: "#F5F8FA",
        border: "1px solid #BFCCD6",
        width: "100%",
        height: "100%"
      }}
    >
      {children}
    </div>
  );
};

export default Dashboard;
