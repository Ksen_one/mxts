enum ControlType {
  Dashboard = 0,
  Input = 1,
  Button = 2,
  Label = 3,
  Grid = 4,
  Tabs = 5,
  Panel = 6
}

export default ControlType;
