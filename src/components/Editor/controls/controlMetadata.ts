export interface ControlMetadata {
  controlName: string;
  icon: string;
  size: ElementSize;
}

export type ElementSize = {
  height: number;
  width: number;
};

export type ElementPosition = {
  x: number;
  y: number;
};