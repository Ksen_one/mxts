import React, { useState, useEffect } from "react";
import { Tab } from "@blueprintjs/core";

import Tabs from "./Tabs";
import { TabsControlInfo } from "../controlInfo";
import { useGraph } from "../../Providers/EditorProviders";

const EditorTabs: React.FC<{ controlInfo: TabsControlInfo }> = ({
  controlInfo: { children: panels },
  ...props
}) => {
  const { state: graph } = useGraph();
  const [tabId, setTabId] = useState(
    (panels.length > 0 && panels[0].clientGuid) || "0"
  );

  useEffect(() => {
    if (!panels || !graph) return;

    const prevPanelCell = panels
      .map(panel => panel.getCell())
      .find(panelCell => panelCell?.visible);

    graph.getModel().beginUpdate();
    panels.forEach(panel => {
      const cell = panel.getCell();
      if (!cell) throw new Error("panel without cell");

      if (panel.clientGuid !== tabId) {
        graph.getModel().setVisible(cell, false);
      } else {
        graph.getModel().setVisible(cell, true);

        if (prevPanelCell && graph.isCellSelected(prevPanelCell)) {
          graph.setSelectionCell(cell);
        }
      }
    });
    graph.getModel().endUpdate();
  }, [graph, tabId, panels]);

  if (panels.length === 0) return null;

  return (
    <Tabs selectedTabId={tabId} onChange={setTabId} {...props}>
      {panels.map(tab => {
        return (
          <Tab
            key={tab.clientGuid}
            id={tab.clientGuid}
            title={tab.clientGuid.split("-")[0]}
          />
        );
      })}
    </Tabs>
  );
};

export default EditorTabs;
