import React from "react";
import { Tabs as BlueprintTabs } from "@blueprintjs/core";

import { ControlMetadata } from "../controlMetadata";

export type TabsProps = {
  selectedTabId?: string;
  onChange?: (guid: string) => void;
};

const Tabs: React.FC<TabsProps> & ControlMetadata = ({
  children,
  selectedTabId,
  onChange = () => {}
}) => {
  if (React.Children.count(children) === 0) return null;
  return (
    <BlueprintTabs
      animate={false}
      selectedTabId={selectedTabId}
      renderActiveTabPanelOnly={true}
      onChange={onChange}
    >
      {children}
    </BlueprintTabs>
  );
};

Tabs.controlName = "Вкладки" as const;
Tabs.icon = "list-detail-view" as const;
Tabs.size = { width: 300, height: 200 };

export default Tabs;
