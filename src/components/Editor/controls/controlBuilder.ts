import {
  DashboardControlInfo,
  TabsControlInfo,
  PanelControlInfo,
  EditorControlInfo
} from "./controlInfo";
import ControlType from "./controlType";

export default class ControlBuilder {
  private static readonly default = (type: ControlType) => (
    parent?: EditorControlInfo
  ) => new EditorControlInfo(type, parent);

  private static readonly creators = {
    [ControlType.Input]: ControlBuilder.default(ControlType.Input),
    [ControlType.Button]: ControlBuilder.default(ControlType.Button),
    [ControlType.Label]: ControlBuilder.default(ControlType.Label),
    [ControlType.Grid]: ControlBuilder.default(ControlType.Grid),
    [ControlType.Panel]: (parent?: EditorControlInfo) => {
      if (!parent || !(parent instanceof TabsControlInfo)) throw new Error();
      return new PanelControlInfo(parent);
    },
    [ControlType.Tabs]: (parent?: EditorControlInfo) =>
      new TabsControlInfo(parent),
    [ControlType.Dashboard]: () => new DashboardControlInfo()
  } as const;

  public static type<T extends ControlType>(controlType: T) {
    const createControl = ControlBuilder.creators[controlType];
    return { createControl };
  }
}
