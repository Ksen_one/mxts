import Input from "./Input/Input";
import Button from "./Button/Button";

import ControlType from "./controlType";
import Tabs from "./Tabs/Tabs";
import Grid from "./Grid/Grid";
import Label from "./Label/Label";

const controls = {
  [ControlType.Input]: Input,
  [ControlType.Button]: Button,
  [ControlType.Label]: Label,
  [ControlType.Grid]: Grid,
  [ControlType.Tabs]: Tabs,
  [ControlType.Dashboard]: null,
  [ControlType.Panel]: null
} as const;

export default controls; 