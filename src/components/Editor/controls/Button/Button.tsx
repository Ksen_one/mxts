import React from "react";
import { Button as BlueprintButton } from "@blueprintjs/core";
import { ControlMetadata } from "../controlMetadata";

type ButtonProps = {
  text?: string;
};

const Button: React.FC<ButtonProps> & ControlMetadata = ({ text }) => {
  return <BlueprintButton fill={true} text={text} style={{ height: "100%" }} />;
};

Button.defaultProps = {
  text: "Default text"
};

Button.controlName = "Кнопка" as const;
Button.icon = "widget-button" as const;
Button.size = { width: 100, height: 40 };

export default Button;

export function withButtonOptions(
  Component: typeof Button,
  config: { text: string }
) {
  return class extends React.Component {
    render() {
      return <Component {...config} />;
    }
  };
}
