import React from "react";
import { Label as BlueprintLabel } from "@blueprintjs/core";
import { ControlMetadata } from '../controlMetadata';

const Label: React.FC & ControlMetadata = () => {
  return <BlueprintLabel>Blueprint Label</BlueprintLabel>;
};

Label.controlName = "Заголовок" as const;
Label.icon = "tag" as const;
Label.size = { width: 100, height: 20 };

export default Label;
