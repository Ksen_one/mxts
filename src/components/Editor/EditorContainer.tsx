import React from "react";

import styles from "./EditorContainer.module.css";

const EditorContainer: React.FC = ({ children }) => (
  <div id="editor" className={styles.container}>
    {children}
  </div>
);

export default EditorContainer;
