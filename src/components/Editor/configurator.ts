import {
  mxRectangle,
  mxRectangleShape,
  mxGraphView,
  mxUtils,
  mxEvent,
  mxKeyHandler,
  mxRubberband,
  mxConstants,
  mxClient,
  mxGraph,
  mxMouseEvent,
  mxGraphHandler,
  mxVertexHandler,
  mxCellHighlight
} from "../../mxgraph";
import Key from "../../services/keyCodes";
import Base64 from "../../services/base64";
import { EditorControlInfo } from "./controls/controlInfo";
import ControlType from "./controls/controlType";

function configPage(
  graph: mxGraph,
  width: number = 1300,
  height: number = 800
) {
  const padding = graph.gridSize * 5;
  const pageWidth = width - (width % graph.gridSize) - padding;
  const pageHeight = height - (height % graph.gridSize) - padding;

  const page = new mxRectangle(0, 0, pageWidth, pageHeight);

  graph.maximumGraphBounds = page;
  graph.pageFormat = page;
  graph.pageVisible = true;
  graph.pageScale = 1;

  graph.view.createBackgroundPageShape = bound => {
    const shape = new mxRectangleShape(bound, "#ffffff", "#d9d9d9");
    (shape as any).strokewidth = 0; // хак
    return shape;
  };
}

// Editor.js 1900
function configBackgroundGrid() {
  // Uses HTML for background pages (to support grid background image)
  mxGraphView.prototype.validateBackgroundPage = function() {
    var graph = this.graph;

    if (graph.container != null && !graph.transparentBackground) {
      if (graph.pageVisible) {
        var bounds = this.getBackgroundPageBounds();

        if (this.backgroundPageShape == null) {
          // Finds first element in graph container
          var firstChild = graph.container.firstChild;

          while (
            firstChild != null &&
            firstChild.nodeType !== mxConstants.NODETYPE_ELEMENT
          ) {
            firstChild = firstChild.nextSibling;
          }

          if (firstChild != null) {
            this.backgroundPageShape = this.createBackgroundPageShape(bounds);
            this.backgroundPageShape.scale = 1;

            // Shadow filter causes problems in outline window in quirks mode. IE8 standards
            // also has known rendering issues inside mxWindow but not using shadow is worse.
            this.backgroundPageShape.isShadow = !(mxClient as any).IS_QUIRKS;
            this.backgroundPageShape.dialect = mxConstants.DIALECT_STRICTHTML;
            this.backgroundPageShape.init(graph.container);

            // Required for the browser to render the background page in correct order
            firstChild.style.position = "absolute";
            graph.container.insertBefore(
              this.backgroundPageShape.node,
              firstChild
            );
            this.backgroundPageShape.redraw();

            this.backgroundPageShape.node.className = "geBackgroundPage";

            // Adds listener for double click handling on background
            (mxEvent as any).addListener(
              this.backgroundPageShape.node,
              "dblclick",
              (mxUtils as any).bind(this, function(evt: any) {
                graph.dblClick(evt);
              })
            );

            // Adds basic listeners for graph event dispatching outside of the
            // container and finishing the handling of a single gesture
            (mxEvent as any).addGestureListeners(
              this.backgroundPageShape.node,
              (mxUtils as any).bind(this, function(evt: any) {
                graph.fireMouseEvent(
                  (mxEvent as any).MOUSE_DOWN,
                  new mxMouseEvent(evt)
                );
              }),
              (mxUtils as any).bind(this, function(evt: any) {
                // Hides the tooltip if mouse is outside container
                if (
                  graph.tooltipHandler != null &&
                  graph.tooltipHandler.isHideOnHover()
                ) {
                  graph.tooltipHandler.hide();
                }

                if (graph.isMouseDown && !(mxEvent as any).isConsumed(evt)) {
                  graph.fireMouseEvent(
                    (mxEvent as any).MOUSE_MOVE,
                    new mxMouseEvent(evt)
                  );
                }
              }),
              (mxUtils as any).bind(this, function(evt: any) {
                graph.fireMouseEvent(
                  (mxEvent as any).MOUSE_UP,
                  new mxMouseEvent(evt)
                );
              })
            );
          }
        } else {
          this.backgroundPageShape.scale = 1;
          this.backgroundPageShape.bounds = bounds;
          this.backgroundPageShape.redraw();
        }
      } else if (this.backgroundPageShape != null) {
        this.backgroundPageShape.destroy();
        this.backgroundPageShape = null;
      }

      this.validateBackgroundStyles();
    }
  };

  // Updates the CSS of the background to draw the grid
  mxGraphView.prototype.validateBackgroundStyles = function() {
    var graph = this.graph;
    var color =
      graph.background === null || graph.background === mxConstants.NONE
        ? graph.defaultPageBackgroundColor
        : graph.background;
    var gridColor =
      color != null && this.gridColor !== color.toLowerCase()
        ? this.gridColor
        : "#b3b3b3";
    var image = "none";
    var position = "";

    if (graph.isGridEnabled()) {
      var phase = 10;

      if ((mxClient as any).IS_SVG) {
        // Generates the SVG required for drawing the dynamic grid
        image = unescape(encodeURIComponent(this.createSvgGrid(gridColor)));
        image = window.btoa ? btoa(image) : (Base64 as any).encode(image, true);
        image = `url(data:image/svg+xml;base64,${image})`;
        phase = graph.gridSize * this.scale * this.gridSteps;
      } else {
        // Fallback to grid wallpaper with fixed size
        image = `url(${this.gridImage})`;
      }

      var x0 = 0;
      var y0 = 0;

      if (graph.view.backgroundPageShape != null) {
        var bds = this.getBackgroundPageBounds();

        x0 = 1 + bds.x;
        y0 = 1 + bds.y;
      }

      // Computes the offset to maintain origin for grid
      position =
        -Math.round(
          phase -
            (mxUtils as any).mod(this.translate.x * this.scale - x0, phase)
        ) +
        "px " +
        -Math.round(
          phase -
            (mxUtils as any).mod(this.translate.y * this.scale - y0, phase)
        ) +
        "px";
    }

    var canvas = graph.view.canvas;

    if (canvas.ownerSVGElement != null) {
      canvas = canvas.ownerSVGElement;
    }

    if (graph.view.backgroundPageShape != null) {
      graph.view.backgroundPageShape.node.style.backgroundPosition = position;
      graph.view.backgroundPageShape.node.style.backgroundImage = image;
      graph.view.backgroundPageShape.node.style.backgroundColor = color;
      graph.container.className = "geDiagramContainer geDiagramBackdrop editor";
      canvas.style.backgroundImage = "none";
      canvas.style.backgroundColor = "";
    } else {
      graph.container.className = "geDiagramContainer";
      canvas.style.backgroundPosition = position;
      canvas.style.backgroundColor = color;
      canvas.style.backgroundImage = image;
    }
  };

  // Returns the SVG required for painting the background grid.
  mxGraphView.prototype.createSvgGrid = function(color: any) {
    var tmp = this.graph.gridSize * this.scale;
    this.gridSteps = 5;
    while (tmp < this.minGridSize) {
      tmp *= 2;
    }

    var tmp2 = this.gridSteps * tmp;

    // Small grid lines
    var d = [];

    for (var i = 1; i < this.gridSteps; i++) {
      var tmp3 = i * tmp;
      d.push(
        "M 0 " +
          tmp3 +
          " L " +
          tmp2 +
          " " +
          tmp3 +
          " M " +
          tmp3 +
          " 0 L " +
          tmp3 +
          " " +
          tmp2
      );
    }

    // KNOWN: Rounding errors for certain scales (eg. 144%, 121% in Chrome, FF and Safari). Workaround
    // in Chrome is to use 100% for the svg size, but this results in blurred grid for large diagrams.
    var size = tmp2;
    var svg =
      '<svg width="' +
      size +
      '" height="' +
      size +
      '" xmlns="' +
      mxConstants.NS_SVG +
      '">' +
      '<defs><pattern id="grid" width="' +
      tmp2 +
      '" height="' +
      tmp2 +
      '" patternUnits="userSpaceOnUse">' +
      '<path d="' +
      d.join(" ") +
      '" fill="none" stroke="' +
      color +
      '" opacity="0.2" stroke-width="1"/>' +
      '<path d="M ' +
      tmp2 +
      " 0 L 0 0 0 " +
      tmp2 +
      '" fill="none" stroke="' +
      color +
      '" stroke-width="1"/>' +
      '</pattern></defs><rect width="100%" height="100%" fill="url(#grid)"/></svg>';

    return svg;
  };
}

function configHotkeys(graph: mxGraph) {
  const keyHandler = new mxKeyHandler(graph);

  const isEditorEvent = (evt: Event) => {
    const editorContainer = document.querySelector("#editor");
    const source = mxEvent.getSource(evt);
    return editorContainer && mxUtils.isAncestorNode(editorContainer, source);
  };

  keyHandler.isGraphEvent = function(evt: Event) {
    return (
      mxKeyHandler.prototype.isGraphEvent.apply(this, [evt]) ||
      isEditorEvent(evt)
    );
  };

  keyHandler.bindKey(Key.DELETE, function() {
    console.log("KEY: delete");
    if (graph.isEnabled()) {
      const selectedCells = graph.getSelectionCells();
      graph.removeCells(selectedCells);
    }
  });
}

function configGraphConstants() {
  // #a7b6c2 #394b59 #7070c0
  mxConstants.DROP_TARGET_COLOR = "#a7b6c2";
  mxConstants.HIGHLIGHT_STROKEWIDTH = 4;
  mxConstants.HANDLE_FILLCOLOR = "#a7b6c2";
  mxConstants.HANDLE_STROKECOLOR = "#394b59";
  mxConstants.HANDLE_SIZE = 6;
  mxConstants.GUIDE_COLOR = "#394b59";
  mxConstants.GUIDE_STROKEWIDTH = 1;
  mxConstants.VERTEX_SELECTION_DASHED = false;
  mxConstants.VERTEX_SELECTION_STROKEWIDTH = 1;
  mxConstants.VERTEX_SELECTION_COLOR = "#394b59";

  mxCellHighlight.prototype.spacing = 0;
}

function configStyles(graph: mxGraph) {
  const vertexDefault = {
    [mxConstants.STYLE_FONTCOLOR]: "#182026",
    [mxConstants.STYLE_FONTSIZE]: 14,
    [mxConstants.STYLE_FONTFAMILY]:
      "-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Open Sans, Helvetica Neue, Icons16, sans-serif",
    [mxConstants.STYLE_VERTICAL_ALIGN]: mxConstants.ALIGN_MIDDLE,
    [mxConstants.STYLE_ALIGN]: mxConstants.ALIGN_LEFT,
    [mxConstants.STYLE_OVERFLOW]: "fill",
    [mxConstants.STYLE_FILLCOLOR]: "#00000000"
  };
  graph.getStylesheet().putDefaultVertexStyle(vertexDefault);

  const tab = {
    [mxConstants.STYLE_SHAPE]: mxConstants.SHAPE_SWIMLANE,
    [mxConstants.STYLE_SWIMLANE_FILLCOLOR]: "#00000010",
    [mxConstants.STYLE_STARTSIZE]: 30,
    [mxConstants.STYLE_FOLDABLE]: 0
  };
  graph.getStylesheet().putCellStyle("tab", tab);

  const dashboard = {
    [mxConstants.STYLE_SHAPE]: "customShape",
    [mxConstants.STYLE_MOVABLE]: 0,
    [mxConstants.STYLE_FILLCOLOR]: "#394b5910"
  };
  graph.getStylesheet().putCellStyle("dashboard", dashboard);
}

function configRubberband(graph: mxGraph) {
  new mxRubberband(graph);
}

function configCellLabel(graph: mxGraph) {
  graph.convertValueToString = function(cell: mxCell<EditorControlInfo>) {
    if (cell.value && cell.value.node && cell.geometry) {
      cell.value.node.style.width = `${cell.geometry.width}px`;
      cell.value.node.style.height = `${cell.geometry.height}px`;
      return cell.value.node;
    }
    return "";
  };
}

function configGraph(graph: mxGraph) {
  configGraphConstants();

  graph.gridSize = 20;
  graph.centerZoom = false;
  graph.graphHandler.scaleGrid = true;
  graph.autoSizeCellsOnAdd = true;
  graph.extendParentsOnAdd = false;
  graph.allowAutoPanning = true;

  graph.setEnabled(true);
  graph.setHtmlLabels(true);
  graph.setDropEnabled(true);
  graph.setPanning(true);
  graph.setTooltips(false);
  graph.setCellsEditable(false);

  configPage(graph);
  configHotkeys(graph);
  configStyles(graph);
  configRubberband(graph);
  configCellLabel(graph);
  configBackgroundGrid();
  configureGraphHandler(graph.graphHandler);
  configureVertexHangler();

  const controlWithChildren: ControlType[] = [
    ControlType.Panel,
    ControlType.Dashboard
  ];

  graph.isValidDropTarget = cell => {
    if (cell && cell.value instanceof EditorControlInfo) {
      return controlWithChildren.includes(cell.value.type);
    }
    return false;
  };

  // FIXME: При первом вычислении, положение считается неправильно
  graph.constrainChild = function(...args) {
    mxGraph.prototype.constrainChild.apply(this, args);
    mxGraph.prototype.constrainChild.apply(this, args);
  };
}

function configureVertexHangler() {
  // Минимальные размеры элементов
  const vertexHandlerUnion = mxVertexHandler.prototype.union;
  mxVertexHandler.prototype.union = function(...args) {
    const defaultResult = vertexHandlerUnion.apply(this, args);

    const minWidth = mxUtils.getNumber(this.state.style, "minWidth", 0);
    defaultResult.width = Math.max(defaultResult.width, minWidth);

    const minHeight = mxUtils.getNumber(this.state.style, "minHeight", 0);
    defaultResult.height = Math.max(defaultResult.height, minHeight);

    return defaultResult;
  };

  // Ограничение ресайза Дашборда только вниз и вправо
  var vertexHandlerRedrawHandles = mxVertexHandler.prototype.redrawHandles;
  mxVertexHandler.prototype.redrawHandles = function(...args) {
    vertexHandlerRedrawHandles.apply(this, args);

    if (this.state.cell.style === "dashboard")
      if (this.sizers != null && this.sizers.length > 7) {
        [0, 1, 2, 3, 5].forEach(
          i => (this.sizers[i].node.style.display = "none")
        );
      }
  };
}

function configureGraphHandler(graphHandler: mxGraphHandler) {
  graphHandler.guidesEnabled = true;
  graphHandler.highlightEnabled = true;
  graphHandler.removeCellsFromParent = false;

  graphHandler.isDelayedSelection = function(cell, me) {
    const isDelayedSelection = mxGraphHandler.prototype.isDelayedSelection;
    const defaultResult = isDelayedSelection.apply(this, [cell, me]);
    const isDashboard =
      cell.value instanceof EditorControlInfo &&
      cell.value.type === ControlType.Dashboard;
    return isDashboard || defaultResult;
  };

  // Не блокируем ивент на дашборде для работы panning и rubberband
  graphHandler.consumeMouseEvent = function(evtName, me) {
    const cell = me.getCell();
    if (
      evtName === "mouseDown" &&
      cell &&
      cell.value instanceof EditorControlInfo &&
      cell.value.type === ControlType.Dashboard
    ) {
      me.state = undefined;
      me.sourceState = undefined;
      return;
    }
    me.consume();
  };
}

export default {
  configGraph
};
