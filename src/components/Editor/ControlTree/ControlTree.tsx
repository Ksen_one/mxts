import React, { useMemo } from "react";
import ReactDOM from "react-dom";

import controls from "../controls";
import { TabsControlInfo, EditorControlInfo } from "../controls/controlInfo";
import ControlType from "../controls/controlType";
import EditorTabs from "../controls/Tabs/EditorTabs";
import { useControlTreeState } from "../Providers/ControlTreeProvider";

const ControlTree: React.FC = () => {
  const { controlTree } = useControlTreeState();

  return <Tree controlTree={controlTree} />;
};

export default ControlTree;

const Tree: React.FC<{ controlTree: EditorControlInfo[] }> = ({
  controlTree
}) => {
  const tree = controlTree.map(control => (
    <Portalable key={control.clientGuid} node={control.node}>
      <ConvertToElement control={control} />
    </Portalable>
  ));

  return <>{tree}</>;
};

function getEditorEnviroment(controlInfo: EditorControlInfo) {
  switch (controlInfo.type) {
    case ControlType.Tabs:
      return () => <EditorTabs controlInfo={controlInfo as TabsControlInfo} />;
    default:
      return;
  }
}

const ConvertToElement: React.FC<{ control: EditorControlInfo }> = ({
  control
}) => {
  const { type, options, children } = control;
  const Element = controls[type];
  const EditorEnvironment = useMemo(() => getEditorEnviroment(control), [
    control
  ]);
  return (
    <>
      {EditorEnvironment ? (
        <EditorEnvironment />
      ) : (
        Element && <Element {...(options[0] as any)} />
      )}
      {children.length > 0 && <Tree controlTree={children} />}
    </>
  );
};

const Portalable: React.FC<{ node: HTMLDivElement }> = ({ children, node }) => {
  return ReactDOM.createPortal(children, node);
};
