import React from "react";

import styles from "Editor.module.css";
import GraphWorkspace from "./GraphWorkspace/GraphWorkspace";
import Toolbar from "./Toolbar";
import Sidebar from "./Sidebar";
import EditorProvider from "./Providers/EditorProviders";
import ContextMenu from "./ContextMenu";
import ControlTree from "./ControlTree/ControlTree";
import Loader from "./Loader/Loader";
import EditorContainer from "./EditorContainer";

const Editor: React.FC<{ guid?: string }> = ({ guid }) => {
  return (
    <EditorProvider>
      <EditorContainer>
        <Loader />
        <ContextMenu />
        <div className={styles.leftColumn}>
          <Sidebar />
        </div>
        <div className={styles.rightColumn}>
          <Toolbar />
          <GraphWorkspace dashboardGuid={guid} />
          <ControlTree />
        </div>
      </EditorContainer>
    </EditorProvider>
  );
};

export default Editor;
