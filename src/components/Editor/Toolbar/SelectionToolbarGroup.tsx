import React from "react";
import { Button, Navbar, Intent } from "@blueprintjs/core";

import { useSelection } from "../Providers/SelectionProvider";
import { useGraph } from "../Providers/EditorProviders";
import { isRemovableSelection } from "../../../services/selectionService";

const SelectionToolbarGroup: React.FC = () => {
  const { state: graph } = useGraph();
  const selection = useSelection();

  if (!graph || selection.length === 0) return null;

  const actions = [];

  if (isRemovableSelection(selection)) {
    actions.push(
      <Button
        minimal
        key="deleteControls"
        intent={Intent.DANGER}
        icon="trash"
        onClick={() => graph.removeCells(selection)}
      />
    );
  }

  if (actions.length === 0) return null;

  return (
    <Navbar.Group>
      <Navbar.Divider />
      {actions}
    </Navbar.Group>
  );
};

export default SelectionToolbarGroup;
