import React from "react";
import {
  Button,
  Navbar,
  OverflowList,
  Popover,
  Position,
  Classes,
  Boundary,
  EditableText,
  NavbarHeading
} from "@blueprintjs/core";

import ZoomToolbarGroup from "./ZoomToolbarGroup";
import SelectionToolbarGroup from "./SelectionToolbarGroup";
import { ExportControlTreeModel } from "./utils/ExportControlTreeModel";
import { ExportGraphModel } from "./utils/ExportGraphModel";

const Toolbar = () => {
  // 20px для корректной работы OverflowList
  const width = "calc(100% - 20px)";
  return (
    <Navbar>
      <OverflowList
        style={{ width }}
        observeParents={true}
        collapseFrom={Boundary.END}
        items={[
          <Navbar.Group key="dataflow">
            <Button minimal icon="refresh" disabled />
            <Button minimal icon="floppy-disk" disabled />
          </Navbar.Group>,
          <Navbar.Group key="utils">
            <Navbar.Divider />
            <Button minimal icon="duplicate" disabled />
            <Button minimal icon="insert" disabled />
            <Button minimal icon="undo" disabled />
            <Button minimal icon="redo" disabled />
          </Navbar.Group>,
          <SelectionToolbarGroup key="selection" />,
          <Navbar.Group key="name" style={{ marginLeft: "auto" }}>
            <NavbarHeading>
              <EditableText
                placeholder="Dashboard name..."
                minWidth={1}
                maxLength={50}
              />
            </NavbarHeading>
          </Navbar.Group>,
          <Navbar.Group key="dev" style={{ marginLeft: "auto" }}>
            <ExportControlTreeModel />
            <ExportGraphModel />
            <Navbar.Divider />
            <ZoomToolbarGroup />
          </Navbar.Group>
        ]}
        overflowRenderer={items => renderOverflowElements(items)}
        visibleItemRenderer={item => item}
      />
    </Navbar>
  );
};

function renderOverflowElements(items: JSX.Element[]) {
  return (
    <Navbar.Group style={{ marginLeft: "auto" }}>
      <Popover
        position={Position.BOTTOM}
        minimal
        modifiers={{ offset: { offset: "0 10px" } }}
      >
        <span className={Classes.BREADCRUMBS_COLLAPSED} />
        <Navbar>{items}</Navbar>
      </Popover>
    </Navbar.Group>
  );
}

export default Toolbar;
