import React from "react";
import { Button } from "@blueprintjs/core";

import { useGraph } from "../../Providers/EditorProviders";

export const ExportGraphModel: React.FC = () => {
  const { state: graph } = useGraph();

  if (!graph) return null;

  const exportJson = () => {
    const model: any = graph.getModel();
    const jsonModel = Object.keys(model.cells)
      .map(cell => {
        const currentCell = model.getCell(cell);
        return currentCell.value !== undefined ? currentCell : null;
      })
      .filter(item => item !== null);
    console.log(jsonModel);
  };

  return <Button minimal={true} icon="layout" onClick={exportJson} />;
};
