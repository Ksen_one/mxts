import React from "react";
import { Button } from "@blueprintjs/core";

import { useControlTreeState } from "../../Providers/ControlTreeProvider";

export const ExportControlTreeModel: React.FC = () => {
  const { controlTree } = useControlTreeState();

  const exportJson = () => {
    console.log(controlTree);
    console.log(controlTree[0]?.toDto());
    console.log(JSON.stringify(controlTree[0]?.toDto(), undefined, 2));
  };

  return (
    <Button minimal={true} icon="document" onClick={exportJson} />
  );
};
