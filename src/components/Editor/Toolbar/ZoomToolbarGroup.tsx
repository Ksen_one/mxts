import React, { useState, useEffect, useCallback } from "react";
import { Button, ControlGroup } from "@blueprintjs/core";

import { useGraph } from "../Providers/EditorProviders";
import { mxEvent } from "../../../mxgraph";

const ZoomToolbarGroup = () => {
  const [zoom, setZoom] = useState(1);
  const { state: graph } = useGraph();

  const zoomIn = useCallback(() => {
    if (!graph) return;
    graph.zoomIn();
    setZoom(zoom * graph.zoomFactor);
  }, [zoom, graph]);

  const zoomOut = useCallback(() => {
    if (!graph) return;
    graph.zoomOut();
    setZoom(zoom / graph.zoomFactor);
  }, [zoom, graph]);

  useEffect(() => {
    if (!graph) return;
    mxEvent.addMouseWheelListener((evt, up) => {
      if (evt.shiftKey && up) {
        zoomIn();
        mxEvent.consume(evt, false);
      } else if (evt.shiftKey) {
        zoomOut();
        mxEvent.consume(evt, false);
      }
    }, window.document.querySelector("#root"));
    return () =>
      mxEvent.removeAllListeners(window.document.querySelector("#root"));
  }, [graph, zoomIn, zoomOut]);

  if (!graph) return null;

  const resetZoom = () => {
    if (!graph) return;
    graph.zoomActual();
    setZoom(1);
  };

  return (
    <ControlGroup>
      <Button minimal={true} icon="zoom-in" onClick={zoomIn} />
      <Button minimal={true} icon="zoom-out" onClick={zoomOut} />
      <Button
        minimal={true}
        icon="zoom-to-fit"
        text={`${Math.ceil(zoom * 100)}%`}
        onClick={resetZoom}
      />
    </ControlGroup>
  );
};

export default ZoomToolbarGroup;
