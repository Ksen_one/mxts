import React, { useEffect, useState } from "react";

import { useGraph } from "./EditorProviders";
import { createCtx } from "../../../services/contextService";
import { mxEvent } from "../../../mxgraph";
import { EditorControlInfo } from "../controls/controlInfo";
import { isControlInfoCell } from "../../../services/graphService";

export const [useSelection, SelectionContextProvider] = createCtx<
  mxCell<EditorControlInfo>[]
>();

const SelectionProvider: React.FC = props => {
  const { state: graph } = useGraph();

  const [selection, setSelection] = useState<mxCell<EditorControlInfo>[]>([]);

  useEffect(() => {
    if (!graph) return;
    graph.getSelectionModel().addListener(mxEvent.CHANGE, (sender, evt) => {
      setSelection(selection => {
        if (selection.length === 0 && sender.cells.length === 0)
          return selection;
        return [...sender.cells.filter(isControlInfoCell)];
      });
    });
  }, [graph]);

  return <SelectionContextProvider value={selection} {...props} />;
};

export default SelectionProvider;
