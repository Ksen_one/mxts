import { EditorControlInfo } from "../../controls/controlInfo";

export function clearControlTree() {
  return { type: "CLEAR_CONTROL_TREE" } as const;
}

export function updateControl(control: EditorControlInfo) {
  return { type: "UPDATE_CONTROL", payload: control } as const;
}

export function addControl(control: EditorControlInfo) {
  return { type: "ADD_CONTROL", payload: control } as const;
}

export function removeControl(control: EditorControlInfo) {
  return { type: "REMOVE_CONTROL", payload: control } as const;
}

export function updateParentControl(
  control: EditorControlInfo,
  parent?: EditorControlInfo
) {
  return {
    type: "UPDATE_CONTROL_PARENT",
    payload: { control, parent }
  } as const;
}
