import {
  createReducerCtx,
  createEditorActions
} from "../../../../services/contextService";
import * as actionCreators from "./controlTreeActionCreators";
import reducer, { initialState } from "./controlTreeReducer";

const [
  useControlTreeState,
  useControlTreeActions,
  ControlTreeProvider
] = createReducerCtx(
  reducer,
  initialState,
  createEditorActions(actionCreators)
);

export { useControlTreeState };

export { useControlTreeActions };

export default ControlTreeProvider;
