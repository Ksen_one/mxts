import * as actionCreators from "./controlTreeActionCreators";
import { EditorControlInfo } from "../../controls/controlInfo";
import { InferValueType } from "../../../../services/contextService";

export type ControlTreeActions = ReturnType<
  InferValueType<typeof actionCreators>
>;

export type ControlTreeState = { controlTree: Array<EditorControlInfo> };
export const initialState: ControlTreeState = { controlTree: [] };

export default function reducer(
  state: ControlTreeState,
  action: ControlTreeActions
) {
  switch (action.type) {
    case "CLEAR_CONTROL_TREE": {
      return { ...initialState };
    }
    case "UPDATE_CONTROL": {
      return { ...state };
    }
    case "ADD_CONTROL": {
      const control = action.payload;
      if (!control.parent) {
        return { controlTree: [...state.controlTree, control] };
      } else {
        return { ...state };
      }
    }
    case "REMOVE_CONTROL": {
      const filterControl = (
        controls: EditorControlInfo[],
        filter: EditorControlInfo
      ) => {
        controls.forEach(ctrl => {
          ctrl.removeChild(filter);
          filterControl(ctrl.children, filter);
        });
      };

      filterControl(state.controlTree, action.payload);
      return { ...state };
    }
    case "UPDATE_CONTROL_PARENT": {
      const { control, parent } = action.payload;
      const prevParent = control.parent;

      if (parent) {
        parent.addChildren([control]);
        control.parent = parent;
      } else {
        control.parent = null;
      }

      if (prevParent) {
        prevParent.removeChild(control);
        return parent
          ? { ...state }
          : { controlTree: [...state.controlTree, control] };
      }
      return {
        controlTree: [...state.controlTree.filter(ctrl => ctrl !== control)]
      };
    }
    default:
      throw new Error();
  }
}
