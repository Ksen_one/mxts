import React, { PropsWithChildren } from "react";

import { createStateCtx } from "../../../services/contextService";
import SelectionProvider from "./SelectionProvider";
import ErrorBoundary from "../ErrorBoundary";
import EditorStateProvider from "./EditorStateProvider";
import ControlTreeProvider from "./ControlTreeProvider";

const [useGraph, GraphProvider] = createStateCtx<mxGraph | null>(null);
export { useGraph };

export default (props: PropsWithChildren<{}>) => {
  return (
    <ErrorBoundary>
      <GraphProvider>
        <ControlTreeProvider>
          <EditorStateProvider>
            <SelectionProvider {...props} />
          </EditorStateProvider>
        </ControlTreeProvider>
      </GraphProvider>
    </ErrorBoundary>
  );
};
