import {
  createReducerCtx,
  InferValueType,
  createEditorActions
} from "../../../services/contextService";

function startLoading() {
  return { type: "LOADING_STARTED" } as const;
}

function finishLoading() {
  return { type: "LOADING_FINISHED" } as const;
}

function criticalError(error: string) {
  return { type: "CRITICAL_ERROR", payload: { error } } as const;
}

const actionCreators = {
  startLoading,
  finishLoading,
  criticalError
};

export type EditorState = { loading: boolean };
export const initialState: EditorState = { loading: false };

function reducer(
  state: EditorState,
  action: ReturnType<InferValueType<typeof actionCreators>>
) {
  switch (action.type) {
    case "LOADING_STARTED":
      return { ...state, loading: true };
    case "LOADING_FINISHED":
      return { ...state, loading: false };
    case "CRITICAL_ERROR":
      throw new Error(action.payload.error);

    default:
      throw new Error();
  }
}

const [
  useEditorState,
  useEditorStateActions,
  EditorStateProvider
] = createReducerCtx(
  reducer,
  initialState,
  createEditorActions(actionCreators)
);

export { useEditorState };
export { useEditorStateActions };

export default EditorStateProvider;
