import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import {
  Classes,
  Popover,
  Position
} from "@blueprintjs/core";

import { useGraph } from "../Providers/EditorProviders";
import ContextMenuContent from "./ContextMenuContent";

const ContextMenu: React.FC = () => {
  const { state: graph } = useGraph();
  const [position, setPosition] = useState<{
    left: string;
    top: string;
  } | null>(null);

  useEffect(() => {
    if (!graph) return;
    // Используем обработчики от mxGraph, нужен хотя бы один элемент
    graph.popupMenuHandler.factoryMethod = menu => {
      menu.addItem("Stub", null, function() {});
    };
  }, [graph]);

  useEffect(() => {
    if (!graph) return;
    graph.popupMenuHandler.showMenu = () => {
      const { left, top } = graph.popupMenuHandler.div.style;
      setPosition({ left, top });
    };
    graph.popupMenuHandler.hideMenu = () => setPosition(null);
  }, [graph]);

  if (!graph || !position) return null;
  const rootNode = document.querySelector("#root");

  if (!rootNode) return null;
  const { top, left } = position;

  return ReactDOM.createPortal(
    <div
      key={`${left}x${top}`}
      className={Classes.CONTEXT_MENU_POPOVER_TARGET}
      style={position}
    >
      <Popover
        content={<ContextMenuContent />}
        target={<div />}
        position={Position.RIGHT_TOP}
        onInteraction={() => setPosition(null)}
        minimal={true}
        isOpen={true}
      />
    </div>,
    rootNode
  );
};

export default ContextMenu;
