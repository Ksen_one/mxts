import React from "react";
import {
  Menu,
  Classes,
  MenuDivider,
  MenuItem,
  Intent
} from "@blueprintjs/core";

import { useGraph } from "../Providers/EditorProviders";
import { useSelection } from "../Providers/SelectionProvider";
import { isRemovableSelection } from "../../../services/selectionService";

const ContextMenuContent: React.FC = () => {
  const { state: graph } = useGraph();
  const selection = useSelection();

  if (!graph || selection.length === 0) return null;

  return (
    <Menu className={Classes.ELEVATION_1}>
      <MenuDivider title="Edit" />
      <MenuItem icon="cut" text="Cut" label="⌘X" />
      <MenuItem icon="duplicate" text="Copy" label="⌘C" />
      <MenuItem icon="clipboard" text="Paste" label="⌘V" disabled={true} />
      {isRemovableSelection(selection) && (
        <MenuItem
          icon="trash"
          text="Delete"
          label="Del"
          intent={Intent.DANGER}
          onClick={() => graph.removeCells(selection)}
        />
      )}
      <MenuDivider title="Text" />
      <MenuItem disabled={true} icon="align-left" text="Alignment">
        <MenuItem icon="align-left" text="Left" />
        <MenuItem icon="align-center" text="Center" />
        <MenuItem icon="align-right" text="Right" />
        <MenuItem icon="align-justify" text="Justify" />
      </MenuItem>
      <MenuItem icon="style" text="Style">
        <MenuItem icon="bold" text="Bold" />
        <MenuItem icon="italic" text="Italic" />
        <MenuItem icon="underline" text="Underline" />
      </MenuItem>
      <MenuItem icon="asterisk" text="Miscellaneous">
        <MenuItem icon="badge" text="Badge" />
        <MenuItem
          icon="book"
          text="Long items will truncate when they reach max-width"
        />
        <MenuItem icon="more" text="Look in here for even more items">
          <MenuItem icon="briefcase" text="Briefcase" />
          <MenuItem icon="calculator" text="Calculator" />
          <MenuItem icon="dollar" text="Dollar" />
          <MenuItem icon="dot" text="Shapes">
            <MenuItem icon="full-circle" text="Full circle" />
            <MenuItem icon="heart" text="Heart" />
            <MenuItem icon="ring" text="Ring" />
            <MenuItem icon="square" text="Square" />
          </MenuItem>
        </MenuItem>
      </MenuItem>
    </Menu>
  );
};

export default ContextMenuContent;
