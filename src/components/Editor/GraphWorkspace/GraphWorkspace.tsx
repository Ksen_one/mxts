import React, { useRef, useEffect } from "react";
import { mxClient, mxGraph, mxEvent } from "../../../mxgraph";

import "./GraphWorkspace.css";
import configurator from "../configurator";
import { useGraph } from "../Providers/EditorProviders";
import {
  resetPanningPosition,
  isMxChildChange
} from "../../../services/graphService";
import {
  createEmptyDashboard,
  restoreGraphControlTree
} from "../../../services/controlService";
import { ControlInfoDto } from "../controls/сontrolInfoDto";
import mock from "./mockControlTree.json";
import ControlType from "../controls/controlType";
import { useControlTreeActions } from "../Providers/ControlTreeProvider";
import { useEditorStateActions } from "../Providers/EditorStateProvider";

const fakeFetch = () =>
  new Promise<ControlInfoDto<ControlType.Dashboard>>(ok => {
    setTimeout(() => {
      ok(mock as any);
    }, 2000);
  });

const GraphWorkspace: React.FC<{ dashboardGuid?: string }> = ({
  dashboardGuid
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const { state: graph, update: setGraph } = useGraph();
  const {
    startLoading,
    finishLoading,
    criticalError
  } = useEditorStateActions();

  const {
    removeControl,
    updateParentControl,
    addControl,
    clearControlTree
  } = useControlTreeActions();

  useEffect(() => {
    if (!ref.current) return;
    if (!mxClient.isBrowserSupported()) {
      criticalError("Browser is not supported!");
      return;
    }

    const container = ref.current;
    mxEvent.disableContextMenu(container);

    const graph = new mxGraph(container);
    configurator.configGraph(graph);

    setGraph(graph);
  }, [criticalError, ref, setGraph]);

  useEffect(() => {
    if (!graph) return;
    // Синхронизация изменения родителя
    graph.getModel().addListener(mxEvent.CHANGE, function(sender, evt) {
      const changes = evt.getProperty("edit").changes;

      for (let i = 0; i < changes.length; i++) {
        const change = changes[i];
        if (isMxChildChange(change)) {
          const control =
            change.child?.value?.type !== 6
              ? change.child.value
              : change.child.value.parent;
          if (!control) continue;
          if (change.parent === null) {
            removeControl(control);
          }
          if (change.previous !== null && change.parent !== null) {
            updateParentControl(control, change.parent.value);
          }
        }
      }
    });
  }, [graph, removeControl, updateParentControl]);

  useEffect(() => {
    if (!graph) return;

    const loadControlTree = async () => {
      try {
        startLoading();
        const controlTreeDto = await fakeFetch();
        const dashboard = restoreGraphControlTree(graph, controlTreeDto);
        addControl(dashboard);
      } catch (error) {
        console.log(error);
        rollback();
        createNewDashboard();
      } finally {
        finishLoading();
        resetPanningPosition(graph);
      }
    };

    const createNewDashboard = () => {
      const dashboard = createEmptyDashboard(graph);
      addControl(dashboard);
      resetPanningPosition(graph);
    };

    const rollback = () => {
      clearControlTree();
      graph.getModel().clear();
      resetPanningPosition(graph);
    };

    rollback();
    dashboardGuid ? loadControlTree() : createNewDashboard();
  }, [
    addControl,
    dashboardGuid,
    graph,
    clearControlTree,
    startLoading,
    finishLoading
  ]);

  return <div className="editor" ref={ref} />;
};

export default GraphWorkspace;
