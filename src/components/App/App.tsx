import React, { useState } from "react";
import { Button } from "@blueprintjs/core";

import Editor from "../Editor";

const App: React.FC = () => {
  const [fakeSelectedDashboard, setDashboard] = useState<string>();
  return (
    <div
      style={{
        display: "flex",
        height: "100%",
        position: "relative",
        background:
          "repeating-linear-gradient(45deg, #E1E8ED, #E1E8ED 20px, #CED9E0 0, #CED9E0 40px)"
      }}
    >
      <div style={{ flex: "0 auto", width: "300px", padding: "30px" }}>
        <Button
          icon="refresh"
          text='Load "new" dashboard'
          onClick={() => setDashboard(undefined)}
        />
        <Button
          icon="refresh"
          text='Load "existing" dashboard'
          onClick={() => setDashboard("0000")}
        />
      </div>
      <div style={{ flex: "1 auto" }}>
        <div style={{ height: "75px" }} />
        <div
          style={{
            height: "calc(100% - 75px)",
            borderLeft: "1px solid black",
            borderTop: "1px solid black"
          }}
        >
          <Editor guid={fakeSelectedDashboard}/>
        </div>
      </div>
    </div>
  );
};

export default App;
