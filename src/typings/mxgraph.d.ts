declare module "mxgraph" {
  export default mxGraphFactory;
}

type factoryProps = { mxImageBasePath: string; mxBasePath: string };
function mxGraphFactory({
  mxImageBasePath,
  mxBasePath
}: factoryProps): mxGraphStatic;

interface mxGraphStatic {
  mxPoint: mxPoint; // full
  mxGeometry: mxGeometry; // full
  mxRectangleShape: mxRectangleShape;
  mxRectangle: mxRectangle;

  mxGraph: mxGraph;
  mxGraphModel: mxGraphModel;
  mxGraphView: mxGraphView;
  mxCell: mxCell;
  mxClient: mxClient;
  mxDragSource: mxDragSource;
  mxPopupMenu: mxPopupMenu;
  mxRubberband: mxRubberband;
  mxUndoManager: mxUndoManager;
  mxCellHighlight: mxCellHighlight;

  mxConstants: mxConstants;
  mxUtils: mxUtils;

  mxEvent: mxEvent;
  mxMouseEvent: mxMouseEvent;

  mxCompactTreeLayout: mxCompactTreeLayout;

  mxPopupMenuHandler: mxPopupMenuHandler;
  mxKeyHandler: mxKeyHandler;
  mxGraphHandler: mxGraphHandler;
  mxVertexHandler: mxVertexHandler;
  mxPanningHandler: mxPanningHandler;
}

/**
 * @link https://jgraph.github.io/mxgraph/docs/js-api/files/util/mxPoint-js.html#mxPoint
 */
interface mxPoint {
  /**
   * @default 0
   */
  x: number;

  /**
   * @default 0
   */
  y: number;

  /**
   * Constructs a new point for the optional x and y coordinates.
   * If no coordinates are given, then the default values for x and y are used.
   */
  new (x?: number, y?: number): mxPoint;

  /**
   * Returns true if the given object equals this point.
   */
  equals(obj: mxPoint): boolean;

  /**
   * Returns a clone of this mxPoint.
   */
  clone(): mxPoint;
}

/**
 * @link https://jgraph.github.io/mxgraph/docs/js-api/files/model/mxGeometry-js.html#mxGeometry
 */
interface mxGeometry extends mxRectangle {
  /**
   * Global switch to translate the points in translate.
   * @default true
   */
  TRANSLATE_CONTROL_POINTS: boolean;

  /**
   * Stores alternate values for x, y, width and height in a rectangle.
   * @see swap to exchange the values.
   * @default null
   */
  alternateBounds?: mxRectangle;

  /**
   * Defines the source `mxPoint` of the edge.
   * This is used if the corresponding edge does not have a source vertex.
   * Otherwise it is ignored.
   * @default null
   */
  sourcePoint?: mxPoint;

  /**
   * Defines the target `mxPoint` of the edge.
   * This is used if the corresponding edge does not have a target vertex.
   * Otherwise it is ignored.
   * @default null
   */
  targetPoint?: mxPoint;

  /**
   * Array of `mxPoints` which specifies the control points along the edge.
   * These points are the intermediate points on the edge, for the endpoints
   * use `targetPoint` and `sourcePoint` or set the terminals of the edge to
   * a non-null value.
   * @default null
   */
  points?: mxPoint[];

  /**
   * For edges, this holds the offset (in pixels) from the position defined by
   * <x> and <y> on the edge.
   * For relative geometries (for vertices), this defines the absolute offset
   * from the point defined by the relative coordinates.
   * For absolute geometries (for vertices), this defines the offset for the label.
   * @default null
   */
  offset?: number;

  /**
   * Specifies if the coordinates in the geometry are to be interpreted as relative coordinates.
   * For edges, this is used to define the location of the edge label relative to the
   * edge as rendered on the display.
   * For vertices, this specifies the relative location inside the bounds of the parent cell.
   * If this is false, then the coordinates are relative to the origin of the parent cell or,
   * for edges, the edge label position is relative to the center of the edge as rendered on screen.
   * @default false
   */
  relative: boolean;

  new (x: number, y: nubmer, width: number, height: number): mxGeometry;
  readonly prototype: mxGeometry;

  /**
   * Swaps the x, y, width and height with the values stored in `alternateBounds`
   * and puts the previous values into `alternateBounds` as a rectangle.
   * This operation is carried-out in-place, that is, using the existing geometry instance.
   * If this operation is called during a graph model transactional change,
   * then the geometry should be cloned before calling this method and setting
   * the geometry of the cell using `mxGraphModel.setGeometry`.
   */
  swap(): void;

  /**
   * Returns the mxPoint representing the source or target point of this edge.
   * This is only used if the edge has no source or target vertex.
   * @param isSource Boolean that specifies if the source or target point should be returned.
   */
  getTerminalPoint(isSource: boolean): mxPoint;

  /**
   * Sets the sourcePoint or targetPoint to the given mxPoint and returns the new point.
   * @param point Point to be used as the new source or target point.
   * @param isSource Boolean that specifies if the source or target point should be set.
   */
  setTerminalPoint(point: mxPoint, isSource: boolean): void;

  /**
   * Rotates the geometry by the given angle around the given center.
   * That is, <x> and <y> of the geometry, the `sourcePoint`, `targetPoint` and all points
   * are translated by the given amount.
   * <x> and <y> are only translated if relative is false.
   * @param angle	Number that specifies the rotation angle in degrees.
   * @param cx	`mxPoint` that specifies the center of the rotation.
   */
  rotate(angle: number, cx: mxPoint): void;

  /**
   * Translates the geometry by the specified amount.
   * That is, <x> and <y> of the geometry, the `sourcePoint`, `targetPoint`
   * and all points are translated by the given amount.
   * <x> and <y> are only translated if relative is false.
   * If `TRANSLATE_CONTROL_POINTS` is false, then points are not modified by this function.
   * @param dx
   * @param dy
   */
  translate(dx: number, dy: number): void;

  /**
   * Scales the geometry by the given amount.
   * That is, <x> and <y> of the geometry, the `sourcePoint`, `targetPoint`
   * and all points are scaled by the given amount.
   * <x>, <y>, <width> and <height> are only scaled if relative is false.
   * If <fixedAspect> is true, then the smaller value is used to scale the width and the height.
   * @param sx	Number that specifies the horizontal scale factor.
   * @param sy	Number that specifies the vertical scale factor.
   * @param fixedAspect	Optional boolean to keep the aspect ratio fixed.
   */
  scale(sx: number, sy: number, fixedAspect?: boolean): void;

  /**
   * Returns true if the given object equals this geometry.
   * @param obj
   */
  equals(obj: mxGeometry): boolean;
}

interface mxEvent {
  CHANGE: "change";

  /**
   * Returns the event’s target or srcElement depending on the browser.
   * @param evt
   */
  getSource(evt: Event): Node;

  /**
   * Disables the context menu for the given element.
   */
  disableContextMenu(node: Node): void;

  /**
   * Installs the given function as a handler for mouse wheel events. The
   * function has two arguments: the mouse event and a boolean that specifies
   * if the wheel was moved up or down.
   * @param funct Handler function that takes the event argument and a boolean up
   * argument for the mousewheel direction.
   * @param target Target for installing the listener in Google Chrome. See
   * https://www.chromestatus.com/features/6662647093133312.
   */
  addMouseWheelListener(
    funct: (evt: WheelEvent, up: boolean) => void,
    target: Element | null
  ): void;

  /**
   * Consumes the given event.
   * @param evt Native event to be consumed.
   * @param preventDefault Optional boolean to prevent the default for the event.  Default is true.
   * @param stopPropagation Option boolean to stop event propagation.  Default is true.
   */
  consume(
    evt: Event,
    preventDefault?: boolean,
    stopPropagation?: boolean
  ): void;

  /**
   * Removes all listeners from the given element.
   */
  removeAllListeners(element: Element | null): void;
}

interface mxGraph extends mxEventSource {
  /**
   * `mxRectangle` that specifies the area in which all cells in the diagram should be placed.
   * Uses in `getMaximumGraphBounds`.
   * Use a width or height of 0 if you only want to give a upper, left corner.
   */
  maximumGraphBounds: mxRectangle;

  /**
   * Specifies the page format for the background page.
   * This is used as the default in `mxPrintPreview` and for painting the background page if
   * `pageVisible` is true and the pagebreaks if `pageBreaksVisible` is true.
   * @default mxConstants.PAGE_FORMAT_A4_PORTRAIT.
   */
  pageFormat: mxRectangle;

  /**
   * Specifies if the background page should be visible.
   * @default false
   */
  pageVisible: boolean;

  /**
   * Specifies the scale of the background page.
   * @default 1.5
   */
  pageScale: number;

  /**
   * Specifies the grid size.
   * @default 10
   */
  gridSize: number;

  /**
   * Specifies if the zoom operations should go into the center of
   * the actual diagram rather than going from top, left.
   * @default true
   */
  centerZoom: boolean;

  /**
   * Specifies the return value for `isDropEnabled`.
   * @default false
   */
  dropEnabled: boolean;

  /**
   * Specifies if `autoSize` style should be applied when cells are added.
   * @default false
   */
  autoSizeCellsOnAdd: boolean;

  /**
   * Specifies if panning via `panGraph` should be allowed to implement autoscroll if
   * no scrollbars are available in `scrollPointToVisible`.
   * To enable panning inside the container, near the edge, set `mxPanningManager.border` to a positive value.
   * @default false
   */
  allowAutoPanning: boolean;

  /**
   * Specifies if parents should be extended according to the `extendParents` switch if cells are added.
   * @default true
   */
  extendParentsOnAdd: boolean;

  /** Holds the mxGraphView that caches the mxCellStates for the cells. */
  view: mxGraphView;

  /** */
  container: Element;

  /** mxGraphHandler */
  graphHandler: mxGraphHandler;

  /** popupMenuHandler */
  popupMenuHandler: mxPopupMenuHandler;

  /**
   * Constructs a new mxGraph in the specified container.
   * @param container	Optional DOM node that acts as a container for the graph.  If this is null then the container can be initialized later using init.
   * @param model	Optional mxGraphModel that constitutes the graph data.
   * @param renderHint	Optional string that specifies the display accuracy and performance.  Default is mxConstants.DIALECT_MIXEDHTML (for IE).
   * @param stylesheet	Optional mxStylesheet to be used in the graph.
   *
   * @link https://jgraph.github.io/mxgraph/docs/js-api/files/view/mxGraph-js.html#mxGraph.mxGraph
   */
  new (
    container?: Node,
    model?: mxGraphModel,
    renderHint?: string,
    stylesheet?: mxStylesheet
  ): mxGraph;
  readonly prototype: mxGraph;

  /**
   * Returns the `mxGraphModel` that contains the cells.
   */
  getModel(): mxGraphModel;

  /**
   * Returns the `mxGraphSelectionModel` that contains the selection.
   */
  getSelectionModel(): mxGraphSelectionModel;

  /**
   * Sets the selection cell.
   * @param cell `mxCell` to be selected.
   */
  setSelectionCell(cell: mxCell): void;

  /**
   * Returns true if the given cell is selected.
   * @param cell
   */
  isCellSelected(cell: mxCell): boolean;

  /**
   * Adds a new vertex into the given parent mxCell using value as the user object and the given coordinates as the mxGeometry of the new vertex.
   * The id and style are used for the respective properties of the new mxCell, which is returned.
   * @param parent mxCell that specifies the parent of the new vertex.
   * @param id Optional string that defines the Id of the new vertex.
   * @param value Object to be used as the user object.
   * @param x Integer that defines the x coordinate of the vertex.
   * @param y Integer that defines the y coordinate of the vertex.
   * @param width Integer that defines the width of the vertex.
   * @param height Integer that defines the height of the vertex.
   * @param style Optional string that defines the cell style.
   * @param relative Optional boolean that specifies if the geometry is relative.  Default is false.
   *
   * @link https://jgraph.github.io/mxgraph/docs/js-api/files/view/mxGraph-js.html#mxGraph.insertVertex
   */
  insertVertex<T>(
    parent: mxCell,
    id: string | undefined | null,
    value: T,
    x: number,
    y: number,
    width: number,
    height: number,
    style?: string,
    relative?: boolean = false
  ): mxCell<T>;

  /**
   * Returns defaultParent or mxGraphView.currentRoot or the first child child of mxGraphModel.root if both are null.
   * The value returned by this function should be used as the parent for new cells (aka default layer).
   */
  getDefaultParent(): mxCell;

  /**
   * Returns the bottom-most cell that intersects the given point (x, y) in the cell hierarchy starting at the given parent.
   * This will also return swimlanes if the given location intersects the content area of the swimlane.
   * If this is not desired, then the hitsSwimlaneContent may be used if the returned cell is a swimlane to determine
   * if the location is inside the content area or on the actual title of the swimlane.
   * @param x X-coordinate of the location to be checked.
   * @param y Y-coordinate of the location to be checked.
   * @param parent mxCell that should be used as the root of the recursion.  Default is current root of the view or the root of the model.
   * @param vertices Optional boolean indicating if vertices should be returned.  Default is true.
   * @param edges Optional boolean indicating if edges should be returned.  Default is true.
   * @param ignoreFn Optional function that returns true if cell should be ignored.  The function is passed the cell state and the x and y parameter.
   */
  getCellAt(
    x: number,
    y: number,
    parent?: mxCell,
    vertices?: boolean,
    edges?: boolean,
    ignoreFn?: (c, x, y) => boolean
  ): mxCell;

  /**
   * Returns the bounds of the visible graph.
   * Shortcut to mxGraphView.getGraphBounds.
   * See also: getBoundingBoxFromGeometry.
   */
  getGraphBounds(): mxRectangle;

  /**
   * Returns the bounding box for the geometries of the vertices in the given array of cells.
   * @param cells	Array of mxCells whose bounds should be returned.
   * @param includeEdges Specifies if edge bounds should be included by computing the bounding box for all points in geometry.  Default is false.
   * @link https://jgraph.github.io/mxgraph/docs/js-api/files/view/mxGraph-js.html#mxGraph.getBoundingBoxFromGeometry
   */
  getBoundingBoxFromGeometry(
    cells: mxCell[],
    includeEdges?: boolean = false
  ): mxRectangle;

  /**
   * Specifies if panning should be enabled.
   * This implementation updates `mxPanningHandler.panningEnabled` in <`panningHandler`>.
   * @param enabled Boolean indicating if panning should be enabled.
   */
  setPanning(enabled: boolean): void;

  /**
   * Specifies if the graph should allow dropping of cells onto or into other cells.
   * @param enabled Boolean indicating if the graph should allow dropping of cells into other cells.
   */
  setDropEnabled(enabled: boolean): void;

  /**
   * Returns `dropEnabled` as a boolean.
   */
  isDropEnabled(): boolean;

  isEnabled(): boolean;

  setTooltips;

  setEnabled;

  setHtmlLabels;

  setCellsEditable;

  /**
   * Returns the first cell from the array of selected `mxCells`.
   */
  getSelectionCell(): mxCell;

  /**
   * Returns the array of selected `mxCells`.
   */
  getSelectionCells(): mxCell[];

  /**
   * Removes the given cells from the graph including all connected edges if includeEdges is true.
   * The change is carried out using cellsRemoved.
   * This method fires `mxEvent.REMOVE_CELLS` while the transaction is in progress.
   * The removed cells are returned as an array.
   * @param cells	Array of `mxCells` to remove.  If null is specified then the selection cells which are deletable are used.
   * @param includeEdges Optional boolean which specifies if all connected edges should be removed as well.  Default is true.
   */
  removeCells(cells: mxCell[], includeEdges?: boolean);

  /**
   * Adds the cells to the parent at the given index, connecting each cell to the optional source and target terminal.
   * The change is carried out using cellsAdded.
   * This method fires `mxEvent.ADD_CELLS` while the transaction is in progress.
   * Returns the cells that were added.
   * @param cells Array of `mxCells` to be inserted.
   * @param parent `mxCell` that represents the new parent.  If no parent is given then the default parent is used.
   * @param index Optional index to insert the cells at.  Default is to append.
   * @param source Optional source `mxCell` for all inserted cells.
   * @param target Optional target `mxCell` for all inserted cells.
   */
  addCells(
    cells: mxCell[],
    parent?: mxCell | null,
    index?: number,
    source?: mxCell,
    target?: mxCell
  ): mxCell[];

  /**
   * Returns the textual representation for the given cell.
   * This implementation returns the nodename or string-representation of the user object.
   * @param {mxCell} cell whose textual representation should be returned.
   */
  convertValueToString(cell: mxCell): string | Element;

  /** @default 1.2 (120%) */
  zoomFactor: number;
  zoomIn(): void;
  zoomOut(): void;
  zoomActual(): void;

  /**
   * Returns the `mxStylesheet` that defines the style.
   */
  getStylesheet(): mxStylesheet;

  /**
   * Returns true if the given cell is a valid drop target for the specified cells.
   * If `splitEnabled` is true then this returns `isSplitTarget` for the given arguments else it returns true if the cell is not collapsed and its child count is greater than 0.
   * @param cell `mxCell` that represents the possible drop target.
   * @param cells	`mxCells` that should be dropped into the target.
   * @param evt	Mouseevent that triggered the invocation.
   */
  isValidDropTarget(cell: mxCell, cells?: mxCell[], evt?: MouseEvent): boolean;

  /**
   * Keeps the given cell inside the bounds returned by `getCellContainmentArea` for its parent,
   * according to the rules defined by `getOverlap` and `isConstrainChild`.
   * This modifies the cell’s geometry in-place and does not clone it.
   * @param cells	`mxCell` which should be constrained.
   * @param sizeFirst	Specifies if the size should be changed first. Default is true.
   */
  constrainChild(cell: mxCell): void;
}

interface mxCell<T = Object> {
  /**
   * Holds the Id.
   * @default null
   */
  id: any;

  /**
   * Holds the user object.
   * @default null
   */
  value?: T;

  /**
   * Holds the `mxGeometry`.
   * @default null
   */
  geometry?: mxGeometry;

  /**
   * Holds the style as a string of the form `(stylename|key=value);`.
   * @default null
   */
  style?: string;

  /**
   * Specifies whether the cell is a vertex.
   * @default false
   */
  vertex: boolean;

  /**
   * Specifies whether the cell is an edge.
   * @default false
   */
  edge: boolean;

  /**
   * Specifies whether the cell is connectable.
   * @default true
   */
  connectable: boolean;

  /**
   * Specifies whether the cell is visible.
   * @default true
   */
  visible: boolean;

  /**
   * Specifies whether the cell is collapsed.
   * @default false
   */
  collapsed: boolean;

  /**
   * Reference to the parent cell.
   */
  parent: mxCell;

  /**
   * Reference to the source terminal.
   */
  source;

  /**
   * Reference to the target terminal.
   */
  target;

  /**
   * Holds the child cells.
   */
  children: mxCell[];

  /**
   * Holds the edges.
   */
  edges;

  /**
   * List of members that should not be cloned inside clone.
   * This field is passed to `mxUtils.clone` and is not made persistent in `mxCellCodec`.
   * This is not a convention for all classes, it is only used in this class to mark
   * transient fields since transient modifiers are not supported by the language.
   */
  mxTransient: string[];

  /**
   * Constructs a new cell to be used in a graph model.
   * This method invokes `onInit` upon completion.
   * @param value Optional object that represents the cell value.
   * @param geometry	Optional `mxGeometry` that specifies the geometry.
   * @param style	Optional formatted string that defines the style.
   */
  new <T = Object>(value?: T, geometry?: mxGeometry, style?: string): mxCell<T>;

  /** Called from within the constructor. */
  onInit(): void;

  /**
   * Inserts the specified child into the child array at the specified index and
   * updates the parent reference of the child.
   * If not `childIndex` is specified then the child is appended to the child array.
   * Returns the inserted child.
   * @param child `mxCell` to be inserted or appended to the child array.
   * @param index Optional integer that specifies the index at which the child should be inserted into the child array.
   */
  insert(child: mxCell, index?: number): mxCell;

  setGeometry(geometry: mxGeometry): void;
}

interface mxRectangle {
  x: number;
  y: number;
  width: number;
  height: number;

  new (x: number, y: number, width: number, height: number): mxRectangle;
}

interface mxShape {
  isShadow: boolean;
  isDashed: boolean;
  node: HTMLElement;
}

/**
 * Extends mxShape to implement a rectangle shape.  This shape is registered under mxConstants.SHAPE_RECTANGLE in mxCellRenderer.
 */
interface mxRectangleShape extends mxShape {
  /**
   * @param strokewidth Default 1
   */
  new (
    bounds: mxRectangle,
    fill: string,
    stroke: string,
    strokewidth?: number = 1
  ): mxRectangleShape;
  bounds: mxRectangle;
}

/**
 * Extends mxEventSource to implement a view for a graph.
 * This class is in charge of computing the absolute coordinates for
 * the relative child geometries, the points for perimeters and edge styles and
 * keeping them cached in mxCellStates for faster retrieval.
 * The states are updated whenever the model or the view state (translate, scale) changes.
 * The scale and translate are honoured in the bounds.
 */
interface mxGraphView {
  /**
   * Specifies the scale.
   * @default 1 (100%).
   */
  scale: number;

  /**
   * mxPoint that specifies the current translation.
   * @default new mxPoint.
   */
  translate: mxPoint;

  /** mxRectangle that caches the scales, translated bounds of the current view. */
  graphBounds: mxRectangle;

  new (graph: mxGraph): mxGraphView;

  /** Creates and returns the shape used as the background page. */
  createBackgroundPageShape(bounds: mxRectangle): mxShape;

  /** Returns true if the event origin is one of the drawing panes or containers of the view. */
  isContainerEvent(evt: mxEvent): boolean;

  /** Returns graphBounds. */
  getGraphBounds(): mxRectangle;

  /** Calls validateBackgroundImage and validateBackgroundPage. */
  validateBackground(): void;

  /** Validates the background image. */
  validateBackgroundImage(): void;

  /** Validates the background page. */
  validateBackgroundPage(): void;

  /**
   * Sets the translation and fires a translate event before calling revalidate followed by mxGraph.sizeDidChange.
   * The translation is the negative of the origin.
   */
  setTranslate(dx: number, dy: number): void;
}

/**
 * Extends `mxEventSource` to implement a graph model.
 * The graph model acts as a wrapper around the cells which are in charge of storing the actual graph datastructure.
 * The model acts as a transactional wrapper with event notification for all changes, whereas the cells contain the atomic operations for updating the actual datastructure.
 *
 * @link https://jgraph.github.io/mxgraph/docs/js-api/files/model/mxGraphModel-js.html#mxGraphModel
 */
interface mxGraphModel extends mxEventSource {
  /**
   * Increments the updateLevel by one.  The event notification is queued until updateLevel reaches 0 by use of endUpdate.
   * All changes on mxGraphModel are transactional, that is, they are executed in a single undoable change on the model (without transaction isolation).  Therefore, if you want to combine any number of changes into a single undoable change, you should group any two or more API calls that modify the graph model between beginUpdate and endUpdate calls
   */
  beginUpdate(): void;

  /**
   * Decrements the updateLevel by one and fires an <undo> event if the updateLevel reaches 0.  This function indirectly fires a <change> event by invoking the notify function on the currentEdit und then creates a new currentEdit using createUndoableEdit.
   * The <undo> event is fired only once per edit, whereas the <change> event is fired whenever the notify function is invoked, that is, on undo and redo of the edit.
   */
  endUpdate(): void;

  /**
   * Sets a new root using createRoot.
   */
  clear(): void;

  /**
   * Returns the `mxGeometry` of the given `mxCell`.
   * @param cell
   */
  getGeometry(cell: mxCell): mxGeometry;

  /**
   * Sets the `mxGeometry` of the given `mxCell`.
   * The actual update of the cell is carried out in `geometryForCellChanged`.
   * The `mxGeometryChange` action is used to encapsulate the change.
   */
  setGeometry(cell: mxCell, geometry: mxGeometry): void;

  /**
   * Returns true if the given `mxCell` is visible.
   */
  isVisible(cell: mxCell): boolean;

  /**
   * Sets the visible state of the given mxCell using mxVisibleChange and adds
   * the change to the current transaction.
   * @param cell
   * @param visible
   */
  setVisible(cell: mxCell, visible: boolean): void;
}

/**
 * Defines the appearance of the cells in a graph.
 * See `putCellStyle` for an example of creating a new cell style.
 * It is recommended to use objects, not arrays for holding cell styles.
 * Existing styles can be cloned using `mxUtils.clone` and turned into a string for debugging using `mxUtils.toString`.
 *
 * #### Default Styles
 * The stylesheet contains two built-in styles, which are used if no style is defined for a cell:
 * - `defaultVertex` - Default style for vertices
 * - `defaultEdge` - Default style for edges
 */
interface mxStylesheet {
  new (): mxStylesheet;

  /**
   * Maps from names to cell styles.
   * Each cell style is a map of key, value pairs.
   */
  styles: { [style: string]: string };

  /**
   * Creates and returns the default vertex style.
   */
  createDefaultVertexStyle(): {};

  /**
   * Creates and returns the default edge style.
   */
  createDefaultEdgeStyle(): {};

  /**
   * Sets the default style for vertices using `defaultVertex` as the stylename.
   */
  putDefaultVertexStyle(style: {}): void;

  /**
   * Sets the default style for edges using defaultEdge as the stylename.
   */
  putDefaultEdgeStyle(style: {}): void;

  /**
   * Returns the default style for vertices.
   */
  getDefaultVertexStyle(): {};

  /**
   * Sets the default style for edges.
   */
  getDefaultEdgeStyle(): {};

  /**
   * Stores the given map of key, value pairs under the given name in styles.
   * @link example - https://jgraph.github.io/mxgraph/docs/js-api/files/view/mxStylesheet-js.html#mxStylesheet.putCellStyle
   * @param name	Name for the style to be stored.
   * @param style	Key, value pairs that define the style.
   */
  putCellStyle(name: string, style: {}): void;

  /**
   * Returns the cell style for the specified stylename or the given defaultStyle if no style can be found for the given stylename.
   * @param name	String of the form [(stylename|key=value);] that represents the style.
   * @param defaultStyle	Default style to be returned if no style can be found.
   */
  getCellStyle(name: string, defaultStyle: {});
}

interface mxClient {
  /** Returns true if the current browser is supported, that is, if `mxClient.IS_VML` or `mxClient.IS_SVG` is true. */
  isBrowserSupported(): boolean;
}

interface mxUtils {
  /**
   * Returns the numeric value for the given key in the given associative array
   * or the given default value (or 0) if the value is null.
   * The value is converted to a numeric value using the Number function.
   * @param array Associative array that contains the value for the key.
   * @param key Key whose value should be returned.
   * @param defaultValue Value to be returned if the value for the given key is null.  Default is 0.
   */
  getNumber<
    T extends { [index in Key]: T[Key] },
    Key extends keyof T,
    K extends PropertyKey,
    D
  >(
    array: T,
    key: K,
    defaultValue: D
  ): K extends Key ? T[K] : D;

  /**
   * Displays the given error message in a new mxWindow of the given width.
   * If close is true then an additional close button is added to the window.
   * The optional icon specifies the icon to be used for the window.
   * Default is mxUtils.errorImage.
   * @param message String specifying the message to be displayed.
   * @param width Integer specifying the width of the window.
   * @param close Optional boolean indicating whether to add a close button.
   * @param icon Optional icon for the window decoration.
   */
  error(message: string, width: number, close?: boolean, icon?: string): never;

  /**
   * Configures the given DOM element to act as a drag source for the specified graph.
   * Returns a a new `mxDragSource`.
   * If `mxDragSource.guideEnabled` is enabled then the x and y arguments must be used in funct to match the preview location.
   * @param element DOM element to make draggable.
   * @param graphF `mxGraph` that acts as the drop target or a function that takes a mouse event and returns the current mxGraph.
   * @param funct 	Function to execute on a successful drop.
   * @param dragElement Optional DOM node to be used for the drag preview.
   * @param dx Optional horizontal offset between the cursor and the drag preview.
   * @param dy Optional vertical offset between the cursor and the drag preview.
   * @param autoscroll Optional boolean that specifies if autoscroll should be used.  Default is mxGraph.autoscroll.
   * @param scalePreview Optional boolean that specifies if the preview element should be scaled according to the graph scale.  If this is true, then the offsets will also be scaled.  Default is `false`.
   * @param highlightDropTargets Optional boolean that specifies if dropTargets should be highlighted.  Default is `true`.
   * @param getDropTarget Optional function to return the drop target for a given location (x, y).  Default is `mxGraph.getCellAt`.
   */
  makeDraggable(
    element: Element,
    graphF: mxGraph | (() => mxGraph),
    funct: (
      graph: mxGraph,
      evt: mxEvent,
      targetCell: mxCell | null,
      x: number,
      y: number
    ) => void,
    dragElement?: Node,
    dx?: number | null,
    dy?: number | null,
    autoscroll?: boolean,
    scalePreview?: boolean,
    highlightDropTargets?: boolean,
    getDropTarget?: (graph: mxGraph, x: number, y: number) => mxCell | null
  ): mxDragSource;

  /**
   * Returns true if the given ancestor is an ancestor of the given DOM node in the DOM.  This also returns true if the child is the ancestor.
   */
  isAncestorNode(ancestor: Node, child: Node): boolean;
}

interface mxDragSource {
  /**
   * Reference to the DOM node which was made draggable.
   */
  element: Node;

  /**
   * Specifies if `mxGuide` should be enabled.
   * @default true
   */
  guidesEnabled: boolean;

  new (element: Node, dropHandler): mxDragSource;

  /**
   * Creates and returns a clone of the <`dragElementPrototype`> or the `element` if the former is not defined.
   * @param evt
   */
  createDragElement(evt: Event): Node;

  /**
   * Stops and removes everything and restores the state of the object.
   */
  reset(): void;
}

/**
 * Base class for objects that dispatch named events.
 * To create a subclass that inherits from mxEventSource, the following code is used.
 */
interface mxEventSource {
  /**
   * Holds the event names and associated listeners in an array.
   * The array contains the event name followed by the respective listener for each registered listener.
   */
  eventListeners;

  /**
   * Specifies if events can be fired.
   * @default true
   */
  eventsEnabled: boolean;

  /**
   * Optional source for events.
   * @default null
   */
  eventSource: Node;

  /**
   * Constructs a new event source.
   * @param {Node} eventSource
   */
  new (eventSource?: Node): mxEventSource;

  /**
   * Returns eventsEnabled.
   */
  isEventsEnabled(): boolean;

  /**
   * Sets eventsEnabled.
   */
  setEventsEnabled(value: boolean): void;

  /**
   * Returns eventSource.
   */
  getEventSource(): Node;

  /**
   * Sets eventSource.
   * @param {Node} value
   */
  setEventSource(value: Node): void;

  /**
   * Binds the specified function to the given event name.
   */
  addListener(
    name: string,
    funct: (sender: this, evt: mxEventObject) => void
  ): void;

  /**
   * Removes all occurrences of the given listener from eventListeners.
   */
  removeListener(funct: (sender, evt) => void): void;

  /**
   * Dispatches the given event to the listeners which are registered for the event.
   * @param {mxEventObject} evt mxEventObject that represents the event.
   * @param {Node} sender Optional sender to be passed to the listener. Default value is the return value of getEventSource.
   */
  fireEvent(evt: mxEventObject, sender?: Node): void;
}

type PopupFactoryMethod = (
  menu: mxPopupMenuHandler,
  cell: mxCell,
  evt: Event
) => void;

interface mxPopupMenuHandler extends mxPopupMenu {
  readonly div: HTMLDivElement;

  /**
   * Reference to the enclosing mxGraph.
   */
  graph: mxGraph;

  /**
   * Specifies if cells should be selected if a popupmenu is displayed for them.
   * @default true
   */
  selectOnPopup: boolean;

  /**
   * Specifies if cells should be deselected if a popupmenu is displayed for the diagram background.
   * @default true
   */
  clearSelectionOnBackground: boolean;

  /**
   * X-coordinate of the mouse down event.
   */
  triggerX: number;

  /**
   * Y-coordinate of the mouse down event.
   */
  triggerY: number;

  /**
   * Screen X-coordinate of the mouse down event.
   */
  screenX: number;

  /**
   * Screen Y-coordinate of the mouse down event.
   */
  screenY: number;

  /**
   * Constructs an event handler that creates a mxPopupMenu.
   * @param graph
   * @param factoryMethod
   */
  new (graph: mxGraph, factoryMethod: PopupFactoryMethod): mxPopupMenuHandler;

  /**
   * Initializes the shapes required for this vertex handler.
   */
  init();

  /**
   * Hook for returning if a cell should be selected for a given mxMouseEvent.
   * This implementation returns selectOnPopup.
   */
  isSelectOnPopup(me);

  /**
   * Handles the event by initiating the panning.
   * By consuming the event all subsequent events of the gesture are redirected to this handler.
   */
  mouseDown(sender, me);

  /**
   * Handles the event by updating the panning on the graph.
   */
  mouseMove(sender, me);

  /**
   * Handles the event by setting the translation on the view or showing the popupmenu.
   */
  mouseUp(sender, me);

  /**
   * Hook to return the cell for the mouse up popup trigger handling.
   */
  getCellForPopupEvent(me);

  /**
   * Destroys the handler and all its resources and DOM nodes.
   */
  destroy();
}

/**
 * Basic popup menu.
 * @link https://jgraph.github.io/mxgraph/docs/js-api/files/util/mxPopupMenu-js.html
 */
interface mxPopupMenu {
  new (factoryMethod: PopupFactoryMethod): mxPopupMenu;

  /**
   * Function that is used to create the popup menu.
   * The function takes the current panning handler,
   * the mxCell under the mouse and the mouse event that triggered the call as arguments.
   */
  factoryMethod: PopupFactoryMethod;

  /**
   * Adds the given item to the given parent item.
   * If no parent item is specified then the item is added to the top-level menu.
   * The return value may be used as the parent argument, ie. as a submenu item.
   * The return value is the table row that represents the item.
   * @param title String that represents the title of the menu item.
   * @param image Optional URL for the image icon.
   * @param funct Function associated that takes a mouseup or touchend event.
   * @param parent Optional item returned by addItem.
   * @param iconCls Optional string that represents the CSS class for the image icon.  IconsCls is ignored if image is given.
   * @param enabled Optional boolean indicating if the item is enabled.  Default is true.
   * @param active Optional boolean indicating if the menu should implement any event handling.  Default is true.
   */
  addItem(
    title: string,
    image: string | null,
    funct: () => void,
    parent?: Object,
    iconCls?: string,
    enabled?: boolean,
    active?: boolean
  ): HTMLTableRowElement;

  /**
   * Shows the menu.
   */
  showMenu(): void;

  /**
   * Removes the menu and all submenus.
   */
  hideMenu(): void;
}

interface mxEventObject {
  /**
   * Holds the name.
   */
  name: string;

  /**
   * Holds the properties as an associative array.
   */
  properties: [];

  /**
   * Holds the consumed state.
   * @default false
   */
  consumed: boolean;

  /**
   * Constructs a new event object with the specified name.
   * An optional sequence of key, value pairs can be appended to define properties.
   * @example new mxEventObject("eventName", key1, val1, .., keyN, valN)
   */
  new (name: string): mxEventObject;

  /**
   * Returns name.
   */
  getName(): string;

  /**
   * Returns properties.
   */
  getProperties(): [];

  /**
   * Returns the property for the given key.
   * @param key
   */
  getProperty(key: string): any;

  /**
   * Returns true if the event has been consumed.
   */
  isConsumed(): boolean;

  /**
   * Consumes the event.
   */
  consume(): void;
}

interface mxKeyHandler {
  /**
   * Reference to the mxGraph associated with this handler.
   */
  graph: mxGraph;

  /**
   * Reference to the target DOM, that is, the DOM node where the key event listeners are installed.
   */
  target: Element;

  /**
   * Maps from keycodes to functions for non-pressed control keys.
   */
  normalKeys: Array<(evt: Event) => void>;

  /**
   * Maps from keycodes to functions for pressed shift keys.
   */
  shiftKeys: Array<(evt: Event) => void>;

  /**
   * Maps from keycodes to functions for pressed control keys.
   */
  controlKeys: Array<(evt: Event) => void>;

  /**
   * Maps from keycodes to functions for pressed control and shift keys.
   */
  controlShiftKeys: Array<(evt: Event) => void>;

  /**
   * Specifies if events are handled.
   * @default true
   */
  enabled: boolean;

  /**
   * Event handler that listens to keystroke events.
   * This is not a singleton, however, it is normally only required once if the target is the document element (default).
   * This handler installs a key event listener in the topmost DOM node and processes all events that originate from descandants of <mxGraph.container> or from the topmost DOM node.
   * The latter means that all unhandled keystrokes are handled by this object regardless of the focused state of the graph.
   *
   * Constructs an event handler that executes functions bound to specific keystrokes.
   * @param graph Reference to the associated mxGraph.
   * @param target Optional reference to the event target. If null, the document element is used as the event target, that is, the object where the key event listener is installed.
   */
  new (graph: mxGraph, target?: Element): mxKeyHandler;

  /**
   * Returns true if events are handled. This implementation returns enabled.
   */
  isEnabled(): boolean;

  /**
   * Enables or disables event handling by updating enabled.
   */
  setEnabled(enabled: boolean): void;

  /**
   * Binds the specified keycode to the given function. This binding is used if the control key is not pressed.
   * @param code Integer that specifies the keycode.
   * @param funct JavaScript function that takes the key event as an argument.
   */
  bindKey(code: number, funct: (evt: Event) => void): void;

  /**
   * Binds the specified keycode to the given function. This binding is used if the shift key is pressed.
   * @param code Integer that specifies the keycode.
   * @param funct JavaScript function that takes the key event as an argument.
   */
  bindShiftKey(code: number, funct: (evt: Event) => void): void;

  /**
   * Binds the specified keycode to the given function. This binding is used if the control key is pressed.
   * @param code Integer that specifies the keycode.
   * @param funct JavaScript function that takes the key event as an argument.
   */
  bindControlKey(code: number, funct: (evt: Event) => void): void;

  /**
   * Binds the specified keycode to the given function. This binding is used if the control and shift key are pressed.
   * @param code Integer that specifies the keycode.
   * @param funct JavaScript function that takes the key event as an argument.
   */
  bindControlShiftKey(code: number, funct: (evt: Event) => void): void;

  /**
   * Returns true if the control key is pressed. This uses `mxEvent.isControlDown`.
   * @param evt Key event whose control key pressed state should be returned.
   */
  isControlDown(evt: Event): boolean;

  /**
   * Returns the function associated with the given key event or null if no function is associated with the given event.
   * @param evt Key event whose associated function should be returned.

   */
  getFunction(evt: Event): (evt: Event) => void;

  /**
   * Returns true if the event should be processed by this handler, that is, if the event source is either the target,
   * one of its direct children, a descendant of the <`mxGraph.container`>, or the mxGraph.cellEditor of the graph.
   * @param evt Key event whose control key pressed state should be returned.
   */
  isGraphEvent(evt: Event): boolean;

  /**
   * Handles the event by invoking the function bound to the respective keystroke if `isEnabledForEvent` returns true
   * for the given event and if `isEventIgnored` returns false, except for escape for which `isEventIgnored` is not invoked.
   * @param evt Key event that represents the keystroke.
   */
  keyDown(evt): unknown;

  /**
   * Returns true if the given event should be handled.
   * `isEventIgnored` is called later if the event is not an escape key stroke, in which case escape is called.
   * This implementation returns true if isEnabled returns true for both, this handler and graph, if the event is not consumed and if `isGraphEvent` returns true.
   * @param evt Key event that represents the keystroke.
   */
  isEnabledForEvent(evt: Event): boolean;

  /**
   * Returns true if the given keystroke should be ignored.  This returns `graph.isEditing()`.
   * @param evt Key event that represents the keystroke.
   */
  isEventIgnored(evt: Event): boolean;

  /**
   * Hook to process ESCAPE keystrokes.
   * This implementation invokes `mxGraph.stopEditing` to cancel the current editing, connecting and/or other ongoing modifications.
   * @param evt Key event that represents the keystroke. Possible keycode in this case is **27** (ESCAPE).
   */
  escape(evt: Event): void;

  /**
   * Destroys the handler and all its references into the DOM.
   * This does normally not need to be called, it is called automatically when the window unloads (in IE).
   */
  destroy(): void;
}

interface mxGraphHandler {
  /**
   * Reference to the enclosing `mxGraph`.
   */
  graph: mxGraph;

  /**
   * Defines the maximum number of cells to paint subhandles for.
   * Default is 50 for Firefox and 20 for IE.
   * Set this to 0 if you want an unlimited number of handles to be displayed.
   * This is only recommended if the number of cells in the graph is limited to a small number, eg.  500.
   */
  maxCells: number;

  /**
   * Specifies if events are handled.
   * @default true
   */
  enabled: boolean;

  /**
   * Specifies if drop targets under the mouse should be enabled.
   * @default true
   */
  highlightEnabled: boolean;

  /**
   * Specifies if cloning by control-drag is enabled.
   * @default true
   */
  cloneEnabled;

  /**
   * Specifies if moving is enabled.
   * @default true
   */
  moveEnabled: boolean;

  /**
   * Specifies if other cells should be used for snapping the right,
   * center or left side of the current selection.
   * @default false
   */
  guidesEnabled;

  /**
   * Holds the `mxGuide` instance that is used for alignment.
   */
  guide: mxGuide;

  /**
   * Stores the x-coordinate of the current mouse move.
   */
  currentDx: number;

  /**
   *  Stores the y-coordinate of the current mouse move.
   */
  currentDy: number;

  /**
   * Specifies if a move cursor should be shown if the mouse is over a movable cell.
   * @default true
   */
  updateCursor: boolean;

  /**
   * Specifies if selecting is enabled.
   * @default true
   */
  selectEnabled: boolean;

  /**
   * Specifies if cells may be moved out of their parents.
   * @default true
   */
  removeCellsFromParent: boolean;

  /**
   * If empty parents should be removed from the model after all child cells have been moved out.
   * @default true
   */
  removeEmptyParents: boolean;

  /**
   * Specifies if drop events are interpreted as new connections if no other drop action is defined.
   * @default false
   */
  connectOnDrop: boolean;

  /**
   * Specifies if the view should be scrolled so that a moved cell is visible.
   * @default true
   */
  scrollOnMove: boolean;

  /**
   * Specifies the minimum number of pixels for the width and height of a selection border.
   * @default 6
   */
  minimumSize: number;

  /**
   * Specifies the color of the preview shape.
   * @default 'black'
   */
  previewColor: string;

  /**
   * Specifies if the graph container should be used for preview.
   * @default false
   */
  htmlPreview: boolean;

  /**
   * Reference to the mxShape that represents the preview.
   */
  shape: mxShape;

  /**
   * Specifies if the grid should be scaled.
   * @default false
   */
  scaleGrid: boolean;

  /**
   * Specifies if the bounding box should allow for rotation.
   * @default true
   */
  rotationEnabled: boolean;

  /**
   * Maximum number of cells for which live preview should be used.
   * `0` - means no live preview
   * @default 0
   */
  maxLivePreview: number;

  new (graph: mxGraph): mxGraphHandler;
  readonly prototype: mxGraphHandler;

  /**
   * Consumes the given mouse event.
   * NOTE: This may be used to enable click events for links in labels on iOS as follows as
   * consuming the initial touchStart disables firing the subsequent click event on the link.
   *
   * @param evtName
   * @param me
   */
  consumeMouseEvent(
    evtName: "mouseDown" | "mouseMove" | "mouseUp",
    me: mxMouseEvent
  ): void;

  /**
   * Returns `enabled`.
   */
  isEnabled(): boolean;

  /**
   * Sets `enabled`.
   */
  setEnabled(value: boolean): void;

  /**
   * Returns `cloneEnabled`.
   */
  isCloneEnabled(): boolean;

  /**
   * Sets `cloneEnabled`.
   */
  setCloneEnabled(value: boolean): void;

  /**
   * Returns `moveEnabled`.
   */
  isMoveEnabled(): boolean;

  /**
   * Sets `moveEnabled`.
   */
  setMoveEnabled(value: boolean): void;

  /**
   * Returns `selectEnabled`.
   */
  isSelectEnabled(): boolean;

  /**
   * Sets `selectEnabled`.
   */
  setSelectEnabled(value: boolean): void;

  /**
   * Returns `removeCellsFromParent`.
   */
  isRemoveCellsFromParent(): boolean;

  /**
   * Sets `removeCellsFromParent`.
   */
  setRemoveCellsFromParent(value: boolean): void;

  /**
   * Hook to return initial cell for the given event.
   */
  getInitialCellForEvent(me: mxMouseEvent): mxCell;

  /**
   * Hook to return true for delayed selections.
   */
  isDelayedSelection(cell: mxCell, me: mxMouseEvent): boolean;

  /**
   * Handles the event by selecing the given cell and creating a handle for it.
   * By consuming the event all subsequent events of the gesture are redirected to this handler.
   */
  mouseDown(sender: this, me: mxMouseEvent): void;

  /**
   * Creates an array of cell states which should be used as guides.
   */
  getGuideStates(): mxCellState[];

  /**
   * Returns the cells to be modified by this handler.
   * This implementation returns all selection cells that are movable, or the given initial cell
   * if the given cell is not selected and movable.
   * This handles the case of moving unselectable or unselected cells.
   * @param initialCell	`mxCell` that triggered this handler.
   */
  getCells(initialCell: mxCell): mxCell[];

  /**
   * Returns the `mxRectangle` used as the preview bounds for moving the given cells.
   */
  getPreviewBounds(cells: mxCell[]): mxRectangle;

  /**
   * Returns the union of the `mxCellStates` for the given array of `mxCells`.
   * For vertices, this method uses the bounding box of the corresponding shape if one exists.
   * The bounding box of the corresponding text label and all controls and overlays are ignored.
   * @see `mxGraphView.getBounds`
   * @see `mxGraph.getBoundingBox`
   * @param cells	Array of `mxCells` whose bounding box should be returned.
   */
  getBoundingBox(cells: mxCell[]): mxCellStates[];

  /**
   * Creates the shape used to draw the preview for the given bounds.
   */
  createPreviewShape(bounds: mxRectangle): void;

  /**
   * Starts the handling of the mouse gesture.
   */
  start(cell: mxCell, x: number, y: number): void;

  /**
   * Returns true if the given cell is currently being moved.
   */
  isCellMoving(cell: mxCell): boolean;

  /**
   * Returns true if the guides should be used for the given `mxMouseEvent`.
   * This implementation returns `mxGuide.isEnabledForEvent`.
   */
  useGuidesForEvent(me: mxMouseEvent): boolean;

  /**
   * Snaps the given vector to the grid and returns the given `mxPoint` instance.
   */
  snap(vector: mxPoint): mxPoint;

  /**
   * Returns an `mxPoint` that represents the vector for moving the cells for the given `mxMouseEvent`.
   */
  getDelta(me: mxMouseEvent): mxPoint;

  /**
   * Hook for subclassers do show details while the handler is active.
   */
  updateHint(me: mxMouseEvent): void;

  /**
   * Hooks for subclassers to hide details when the handler gets inactive.
   */
  removeHint(): void;

  /**
   * Hook for rounding the unscaled vector.  This uses `Math.round`.
   */
  roundLength(length: number): number;

  /**
   * Handles the event by highlighting possible drop targets and updating the preview.
   */
  mouseMove(sender: this, me: mxMouseEvent): void;

  /**
   * Updates the bounds of the preview shape.
   */
  updatePreviewShape(): void;

  /**
   * Updates the bounds of the preview shape.
   */
  updateLivePreview(dx: number, dy: number): void;

  /**
   * Redraws the preview shape for the given states array.
   */
  redrawHandles(states: mxCellState[]): void;

  /**
   * Resets the given preview states array.
   */
  resetPreviewStates(states: mxCellState): void;

  /**
   * Resets the livew preview.
   */
  resetLivePreview(): void;

  /**
   * Resets the state of this handler.
   */
  reset(): void;

  /**
   * Sets the color of the rectangle used to highlight drop targets.
   * @param color	String that represents the new highlight color.
   */
  setHighlightColor(color: string): void;

  /**
   * Handles the event by applying the changes to the selection cells.
   */
  mouseUp(sender: this, me: mxMouseEvent): void;

  /**
   * Implements the delayed selection for the given mouse event.
   */
  selectDelayed(me: mxMouseEvent): void;

  /**
   * Returns true if the given cells should be removed from the parent for the specified mousereleased event.
   */
  shouldRemoveCellsFromParent(
    parent: mxCell,
    cells: mxCell,
    evt: Event
  ): boolean;

  /**
   * Moves the given cells by the specified amount.
   */
  moveCells(
    cells: mxCell[],
    dx: number,
    dy: number,
    clone: boolean,
    target: mxCell,
    evt: Event
  ): void;

  /**
   * Destroy the preview and highlight shapes.
   */
  destroyShapes(): void;

  /**
   * Destroys the handler and all its resources and DOM nodes.
   */
  destroy(): void;
}

/**
 * Event handler for resizing cells.
 * This handler is automatically created in `mxGraph.createHandler`.
 * @link https://jgraph.github.io/mxgraph/docs/js-api/files/handler/mxVertexHandler-js.html
 */
interface mxVertexHandler {
  readonly sizers: mxRectangleShape[];

  /**
   * Reference to the enclosing `mxGraph`.
   */
  graph: mxGraph;

  /**
   * Reference to the `mxCellState` being modified.
   */
  state: mxCellState;

  /**
   * Specifies if only one sizer handle at the bottom, right corner should be used.
   * @default false
   */
  singleSizer: boolean;

  /**
   * Holds the index of the current handle.
   */
  index: number;

  /**
   * Specifies if the bounds of handles should be used for hit-detection in IE or if tolerance > 0.
   * @default true
   */
  allowHandleBoundsCheck: boolean;

  /**
   * Optional `mxImage` to be used as handles.
   * @default null
   */
  handleImage: mxImage;

  /**
   * Optional tolerance for hit-detection in getHandleForEvent.
   * @default 0
   */
  tolerance: number;

  /**
   * Specifies if a rotation handle should be visible.
   * @default false
   */
  rotationEnabled: boolean;

  /**
   * Specifies if the parent should be highlighted if a child cell is selected.
   * @default false
   */
  parentHighlightEnabled: boolean;

  /**
   * Specifies if rotation steps should be “rasterized” depening on the distance to the handle.
   * @default true
   */
  rotationRaster: boolean;

  /**
   * Specifies the cursor for the rotation handle.
   * @default 'crosshair'
   */
  rotationCursor: string;

  /**
   * Specifies if resize should change the cell in-place.
   * This is an experimental feature for non-touch devices.
   * @default false
   */
  livePreview: boolean;

  /**
   * Specifies if sizers should be hidden and spaced if the vertex is small.
   * @default false
   */
  manageSizers: boolean;

  /**
   * Specifies if the size of groups should be constrained by the children.
   * @default false
   */
  constrainGroupByChildren: boolean;

  /**
   * Vertical spacing for rotation icon.
   * @default -16
   */
  rotationHandleVSpacing: number;

  /**
   * The horizontal offset for the handles.
   * This is updated in `redrawHandles` if `manageSizers` is true and the sizers
   * are offset horizontally.
   */
  horizontalOffset: number;

  /**
   * The horizontal offset for the handles.
   * This is updated in `redrawHandles` if `manageSizers` is true and the sizers
   * are offset vertically.
   */
  verticalOffset: number;

  new (state: mxCellState): mxVertexHandler;
  readonly prototype: mxVertexHandler;

  /**
   * Initializes the shapes required for this vertex handler.
   */
  init(): void;

  /**
   * Returns true if the rotation handle should be showing.
   */
  isRotationHandleVisible(): boolean;

  /**
   * Returns true if the aspect ratio if the cell should be maintained.
   */
  isConstrainedEvent(me: mxMouseEvent): boolean;

  /**
   * Returns true if the center of the vertex should be maintained during the resize.
   */
  isCenteredEvent(state: mxCellState, me: mxMouseEvent): boolean;

  /**
   * Returns an array of custom handles.
   * This implementation returns null.
   */
  createCustomHandles(): any[] | null;

  /**
   * Initializes the shapes required for this vertex handler.
   */
  updateMinBounds(): void;

  /**
   * Returns the `mxRectangle` that defines the bounds of the selection border.
   */
  getSelectionBounds(): mxRectangle;

  /**
   * Creates the shape used to draw the selection border.
   */
  createParentHighlightShape(bounds: mxRectangle): void;

  /**
   * Creates the shape used to draw the selection border.
   */
  createSelectionShape(bounds: mxRectangle): void;

  /**
   * Returns `mxConstants.VERTEX_SELECTION_COLOR`.
   */
  getSelectionColor(): string;

  /**
   * Returns `mxConstants.VERTEX_SELECTION_STROKEWIDTH`.
   */
  getSelectionStrokeWidth(): number;

  /**
   * Returns `mxConstants.VERTEX_SELECTION_DASHED`.
   */
  isSelectionDashed(): boolean;

  /**
   * Creates a sizer handle for the specified cursor and index and returns
   * the new `mxRectangleShape` that represents the handle.
   */
  createSizer(cursor, index, size, fillColor): mxRectangleShape;

  /**
   * Returns true if the sizer for the given index is visible.
   */
  isSizerVisible(index: number): boolean;

  /**
   * Creates the shape used for the sizer handle for the specified bounds an index.
   * Only images and rectangles should be returned if support for HTML labels with not foreign objects is required.
   */
  createSizerShape(bounds, index, fillColor);

  /**
   * Helper method to create an `mxRectangle` around the given centerpoint with
   * a width and height of 2*s or 6, if no s is given.
   */
  createBounds(): mxRectangle;

  /**
   * Returns the index of the handle for the given event.
   */
  getHandleForEvent(me: mxMouseEvent): number;

  /**
   * Returns true if the given event allows custom handles to be changed.
   */
  isCustomHandleEvent(me: mxMouseEvent): boolean;

  /**
   * Handles the event if a handle has been clicked.
   */
  mouseDown(sender: this, me: mxMouseEvent): void;

  /**
   * Called if `livePreview` is enabled to check if a border should be painted.
   * This implementation returns true if the shape is transparent.
   */
  isLivePreviewBorder(): boolean;

  /**
   * Starts the handling of the mouse gesture.
   */
  start(x: number, y: number, index: number): void;

  /**
   * Shortcut to `hideSizers`.
   */
  hideHandles(): void;

  /**
   * Hides all sizers except.
   * Starts the handling of the mouse gesture.
   */
  hideSizers(): void;

  /**
   * Checks if the coordinates for the given event are within the `mxGraph.tolerance`.
   */
  checkTolerance(me: mxMouseEvent): void;

  /**
   * Hook for subclassers do show details while the handler is active.
   */
  updateHint(me: mxMouseEvent): void;

  /**
   * Hooks for subclassers to hide details when the handler gets inactive.
   */
  removeHint(): void;

  /**
   * Hook for rounding the angle.
   */
  roundAngle(angle: number): void;

  /**
   * Hook for rounding the unscaled width or height.
   */
  roundLength(length: number): void;

  /**
   * Handles the event by updating the preview.
   */
  mouseMove(sender: this, me: mxMouseEvent): void;

  /**
   * Rotates the vertex.
   */
  rotateVertex(me: mxMouseEvent): void;

  /**
   * Repaints the live preview.
   */
  updateLivePreview(me: mxMouseEvent): void;

  /**
   * Handles the event by applying the changes to the geometry.
   */
  mouseUp(sender: this, me: mxMouseEvent): void;

  /**
   * Rotates the given cell to the given rotation.
   */
  rotateCell(): void;

  /**
   * Hook for subclassers to implement a single click on the rotation handle.
   */
  rotateClick(): void;

  /**
   * Rotates the given cell and its children by the given angle in degrees.
   */
  rotateCell(cell: mxCell, angle: number, parent: mxCell): void;

  /**
   * Resets the state of this handler.
   */
  reset(): void;

  /**
   * Uses the given vector to change the bounds of the given cell in the graph
   *  using `mxGraph.resizeCell`.
   */
  resizeCell(
    cell: mxCell,
    dx: number,
    dy: number,
    index: number,
    gridEnabled: boolean,
    constrained: boolean,
    recurse: boolean
  ): void;

  /**
   * Moves the children of the given cell by the given vector.
   */
  moveChildren(cell: mxCell, x: number, y: number): void;

  /**
   * Returns the union of the given bounds and location for the specified handle index.
   */
  union(
    bounds: mxRectangle,
    dx: number,
    dy: number,
    index: number,
    gridEnabled: boolean,
    scale: number,
    tr: boolean,
    constrained: boolean,
    centered: boolean
  ): mxRectangle;

  /**
   * Redraws the handles and the preview.
   */
  redraw(ignoreHandles: boolean): void;

  /**
   * Redraws the handles.
   */
  redrawHandles(): void;

  /**
   * Returns an `mxPoint` that defines the rotation handle position.
   */
  getRotationHandlePosition(): void;

  /**
   * Updates the highlight of the parent if `parentHighlightEnabled` is true.
   */
  updateParentHighlight(): void;

  /**
   * Redraws the preview.
   */
  drawPreview(): void;

  /**
   * Destroys the handler and all its resources and DOM nodes.
   */
  destroy(): void;
}

interface mxRubberband {
  /**
   * Specifies the default opacity to be used for the rubberband div.
   * @default 20
   */
  defaultOpacity: number;

  /**
   * Specifies if events are handled.
   * @default true
   */
  enabled: boolean;

  /**
   * Holds the DIV element which is currently visible.
   */
  div: HTMLDivElement;

  /**
   * Holds the DIV element which is used to display the rubberband.
   */
  sharedDiv: HTMLDivElement;

  /**
   * Holds the value of the x argument in the last call to update.
   */
  currentX: number;

  /**
   * Holds the value of the y argument in the last call to update.
   */
  currentY: number;

  /**
   * Optional fade out effect.
   * @default false
   */
  fadeOut: boolean;

  /**
   * Event handler that selects rectangular regions.
   * This is not built-into `mxGraph`.
   * Constructs an event handler that selects rectangular regions in the graph using rubberband selection.
   */
  new (graph: mxGraph): mxRubberband;

  /**
   * Returns true if events are handled.
   * This implementation returns enabled.
   */
  isEnabled();

  /**
   * Enables or disables event handling.
   * This implementation updates enabled.
   */
  setEnabled(enabled: boolean);

  /**
   * Returns true if the given `mxMouseEvent` should start rubberband selection.
   * This implementation returns true if the alt key is pressed.
   */

  isForceRubberbandEvent(me: mxMouseEvent): boolean;

  /**
   * Handles the event by initiating a rubberband selection.
   * By consuming the event all subsequent events of the gesture are redirected to this handler.
   */
  mouseDown(sender, me: mxMouseEvent): unknown;

  /**
   * Sets the start point for the rubberband selection.
   */
  start(x: number, y: number): void;

  /**
   * Handles the event by updating therubberband selection.
   */
  mouseMove(sender, me: mxMouseEvent): unknown;

  /**
   * Creates the rubberband selection shape.
   */
  createShape(): mxShape;

  /**
   * Returns true if this handler is active.
   */
  isActive(sender, me: mxMouseEvent): boolean;

  /**
   * Handles the event by selecting the region of the rubberband using mxGraph.selectRegion.
   */
  mouseUp(sender, me: mxMouseEvent): unknown;

  /**
   * Resets the state of this handler and selects the current region for the given event.
   */
  execute(evt: Event): void;

  /**
   * Resets the state of the rubberband selection.
   */
  reset(): void;

  /**
   * Sets `currentX` and `currentY` and calls repaint.
   */
  update(x: number, y: number): void;

  /**
   * Computes the bounding box and updates the style of the div.
   */
  repaint(): void;

  /**
   * Destroys the handler and all its resources and DOM nodes.
   * This does normally not need to be called, it is called automatically when the window unloads.
   */
  destroy(): void;
}

interface mxConstants {
  /** DOM node of type ELEMENT. */
  NODETYPE_ELEMENT: 1;
  /** Defines the strict HTML display dialect. */
  DIALECT_STRICTHTML: "strictHtml";
  /** Defines the value for none.  Default is “none”. */
  NONE: "none";
  /** Defines the SVG namespace. */
  NS_SVG: "http://www.w3.org/2000/svg";

  /**
   * Defines the color to be used for the highlighting target parent cells (for drag and drop).
   * Use `'none'` for no color.
   * @default '#0000FF'
   */
  DROP_TARGET_COLOR: string;

  /**
   * Defines the strokewidth to be used for the highlights.  Default is 3.
   */
  HIGHLIGHT_STROKEWIDTH: number;

  /**
   * Defines the color to be used for the handle fill color.  Use ‘none’ for no color.  Default is #00FF00 (green).
   */
  HANDLE_FILLCOLOR: string;

  /**
   * Defines the color to be used for the handle stroke color.  Use ‘none’ for no color.  Default is black.
   */
  HANDLE_STROKECOLOR: string;

  /**
   * Defines the default size for handles.  Default is 6.
   */
  HANDLE_SIZE: number;

  /**
   * Defines the default size for label handles.  Default is 4.
   */
  LABEL_HANDLE_SIZE: number;

  /**
   * Defines the color to be used for the guidelines in mxGraphHandler.
   * @default '#FF0000'
   */
  GUIDE_COLOR: string;

  /**
   * Defines the strokewidth to be used for the guidelines in mxGraphHandler.
   * @default 1
   */
  GUIDE_STROKEWIDTH: number;

  /**
   * Defines the strokewidth to be used for vertex selections.
   * @default 1
   */
  VERTEX_SELECTION_STROKEWIDTH: number;

  /**
   * Defines the dashed state to be used for the vertex selection border.
   * @default true
   */
  VERTEX_SELECTION_DASHED: boolean;

  /**
   * Defines the color to be used for the selection border of vertices.
   * Use ‘none’ for no color.
   * @default '#00FF00'
   */
  VERTEX_SELECTION_COLOR: string;

  /**
   * Defines the key for the shape.
   * Possible values are all constants with a SHAPE-prefix or any newly defined shape names.
   * @value `'shape'`
   */
  STYLE_SHAPE: "shape";

  /**
   * Defines the key for the movable style.  This specifies if a cell can be moved.  Possible values are 0 or 1.  Default is 1.  See mxGraph.isCellMovable.  Value is “movable”.
   */
  STYLE_MOVABLE: "movable";

  /**
   * Defines the key for the verticalAlign style.
   * Possible values are `ALIGN_TOP`, `ALIGN_MIDDLE` and `ALIGN_BOTTOM`.
   * This value defines how the lines of the label are vertically aligned.
   * - `ALIGN_TOP` means the topmost label text line is aligned against the top of the label bounds,
   * - `ALIGN_BOTTOM` means the bottom-most label text line is aligned against the bottom of the label bounds and
   * - `ALIGN_MIDDLE` means there is equal spacing between the topmost text label line and the top of the label bounds and the bottom-most text label line and the bottom of the label bounds.
   *
   * Note this value doesn’t affect the positioning of the overall label bounds relative to the vertex, to move the label bounds vertically, use `STYLE_VERTICAL_LABEL_POSITION`.
   * @value `'verticalAlign'`.
   */
  STYLE_VERTICAL_ALIGN: "verticalAlign";

  /**
   * Defines the key for the align style.
   * Possible values are `ALIGN_LEFT`, `ALIGN_CENTER` and `ALIGN_RIGHT`.
   * This value defines how the lines of the label are horizontally aligned.
   * - `ALIGN_LEFT` mean label text lines are aligned to left of the label bounds,
   * - `ALIGN_RIGHT` to the right of the label bounds and
   * - `ALIGN_CENTER` means the center of the text lines are aligned in the center of the label bounds.
   *
   * Note this value doesn’t affect the positioning of the overall label bounds relative to the vertex, to move the label bounds horizontally, use `STYLE_LABEL_POSITION`.
   * @value `'align'`
   */
  STYLE_ALIGN: "align";

  /**
   * Defines the key for the overflow style.
   * Possible values are `‘visible’`, `‘hidden’`, `‘fill’` and `‘width’`.
   * The default value is `‘visible’`.
   * This value specifies how overlapping vertex labels are handled.
   * - A value of `‘visible’` will show the complete label.
   * - A value of `‘hidden’` will clip the label so that it does not overlap the vertex bounds.
   * - A value of `‘fill’` will use the vertex bounds and a value of ‘width’ will use the the vertex width for the label.
   * @see `mxGraph.isLabelClipped`.
   *
   * Note that the vertical alignment is ignored for overflow fill and for horizontal alignment, left should be used to avoid pixel offsets in Internet Explorer 11 and earlier or if foreignObjects are disabled.
   * @value `'overflow'`
   */
  STYLE_OVERFLOW: "overflow";

  /**
   * Defines the key for the fill color.
   * Possible values are all HTML color names or HEX codes, as well as special keywords such as ‘swimlane, ‘inherit’ or ‘indicated’ to use the color code of a related cell or the indicator shape.
   * @value `'fillColor'`
   */
  STYLE_FILLCOLOR: "fillColor";

  /**
   * Defines the key for the fill color of the swimlane background.
   * Possible values are all HTML color names or HEX codes.
   * Default is no background.
   * @value `'swimlaneFillColor'`
   */
  STYLE_SWIMLANE_FILLCOLOR: "swimlaneFillColor";

  /**
   * Defines the key for the startSize style.  The type of this value is numeric and the value represents the size of the start marker or the size of the swimlane title region depending on the shape it is used for.  Value is “startSize”.
   */
  STYLE_STARTSIZE: "startSize";

  /**
   * Defines the key for the foldable style.  This specifies if a cell is foldable using a folding icon.  Possible values are 0 or 1.  Default is 1.  See mxGraph.isCellFoldable.  Value is “foldable”.
   */
  STYLE_FOLDABLE: "foldable";

  /**
   * Defines the key for the fontColor style.  Possible values are all HTML color names or HEX codes.  Value is “fontColor”.
   */
  STYLE_FONTCOLOR: "fontColor";

  /**
   * Defines the key for the fontSize style (in px).  The type of the value is int.  Value is “fontSize”.
   */
  STYLE_FONTSIZE: "fontSize";

  /**
   * Defines the key for the fontFamily style.  Possible values are names such as Arial; Dialog; Verdana; Times New Roman.  The value is of type String.  Value is fontFamily.
   */
  STYLE_FONTFAMILY: "fontFamily";

  /**
   * Name under which `mxRectangleShape` is registered in `mxCellRenderer`.
   * @default 'rectangle'
   */
  SHAPE_RECTANGLE: "rectangle";

  /**
   * Name under which `mxSwimlane` is registered in `mxCellRenderer`.
   * @default 'swimlane'
   */
  SHAPE_SWIMLANE: "swimlane";

  /**
   * Constant for middle vertical alignment.
   * @default 'middle'
   */
  ALIGN_MIDDLE: "middle";

  /**
   * Constant for top vertical alignment.
   * @default 'top'
   */
  ALIGN_TOP: "top";

  /**
   * Constant for bottom vertical alignment.
   * @default 'bottom'
   */
  ALIGN_BOTTOM: "bottom";

  /**
   * Constant for center horizontal alignment.
   * @default 'center'
   */
  ALIGN_CENTER: "center";

  /**
   * Constant for left horizontal alignment.
   * @default 'left'
   */
  ALIGN_LEFT: "left";

  /**
   * Constant for right horizontal alignment.
   * @default 'right'
   */
  ALIGN_RIGHT: "right";
}

interface mxGraphSelectionModel extends mxEventSource {
  cells: mxCell[];
}

interface mxChildChange {
  new (): mxChildChange;
}

interface mxMouseEvent {
  /**
   * Holds the consumed state of this event.
   */
  consumed: boolean;

  /**
   * Holds the inner event object.
   */
  evt: Event;

  /**
   * Holds the x-coordinate of the event in the graph.
   * This value is set in `mxGraph.fireMouseEvent`.
   */
  graphX: number;

  /**
   * Holds the y-coordinate of the event in the graph.
   * This value is set in `mxGraph.fireMouseEvent`.
   */
  graphY: number;

  /**
   * Holds the optional `mxCellState` associated with this event.
   */
  state: mxCellState;

  /**
   * Holds the `mxCellState` that was passed to the constructor.
   * This can be different from state depending on the result of `mxGraph.getEventState`.
   */
  sourceState: mxCellState;

  new (evt: MouseEvent, state?: mxCellState): mxMouseEvent;
  readonly prototype: mxMouseEvent;

  /**
   * Returns the mxCell in state is not null.
   */
  getCell(): mxCell | null;

  /**
   * Returns evt.
   */

  getEvent(): MouseEvent;

  /**
   * Sets consumed to true and invokes preventDefault on the native event if such a method is defined.
   * This is used mainly to avoid the cursor from being changed to a text cursor in Webkit.
   * You can use the `preventDefault` flag to disable this functionality.
   * @param preventDefault Specifies if the native event should be canceled.  Default is true.
   */
  consume(preventDefault?: boolean): void;
}
