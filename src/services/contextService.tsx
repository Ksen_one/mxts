import React, { useReducer } from "react";

export type InferValueType<T> = T extends { [key: string]: infer U }
  ? U
  : never;

export function createReducerCtx<
  StateType,
  ActionType,
  ActionCreators extends (dispatch: React.Dispatch<ActionType>) => any
>(
  reducer: React.Reducer<StateType, ActionType>,
  initialState: StateType,
  createActions: ActionCreators
) {
  const StateContext = React.createContext({ ...initialState });
  const ActionsContext = React.createContext<ReturnType<typeof createActions>>(
    {} as any
  );

  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, dispatch] = useReducer<React.Reducer<StateType, ActionType>>(
      reducer,
      initialState
    );

    const actions = React.useMemo(() => createActions(dispatch), []);

    return (
      <StateContext.Provider value={state}>
        <ActionsContext.Provider value={actions} {...props} />
      </StateContext.Provider>
    );
  }
  const useContextState = () => React.useContext(StateContext);
  const useContextActions = () => React.useContext(ActionsContext);
  return [useContextState, useContextActions, Provider] as const;
}

export function createStateCtx<T>(defaultValue: T) {
  type UpdateType = React.Dispatch<React.SetStateAction<typeof defaultValue>>;
  const defaultUpdate: UpdateType = () => defaultValue;
  const ctx = React.createContext({
    state: defaultValue,
    update: defaultUpdate
  });
  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, update] = React.useState(defaultValue);
    return <ctx.Provider value={{ state, update }} {...props} />;
  }
  const useContextState = () => React.useContext(ctx);
  return [useContextState, Provider] as const;
}

export function createCtx<T>() {
  const ctx = React.createContext<T | undefined>(undefined);
  function useCtx() {
    const c = React.useContext(ctx);
    if (!c) throw new Error("useCtx must be inside a Provider with a value");
    return c;
  }
  return [useCtx, ctx.Provider] as const;
}

type DispatchableActionCreators<
  T extends { [k: string]: (...args: any[]) => any }
> = {
  [K in keyof T]: (...a: Parameters<T[K]>) => void;
};

export function createEditorActions<
  T extends { [i: string]: (...args: any[]) => any }
>(actionCreators: T) {
  return function createEditorActions(
    dispatch: React.Dispatch<ReturnType<InferValueType<T>>>
  ) {
    const actionCreatorsNames = Object.keys(actionCreators) as [
      keyof typeof actionCreators
    ];

    return actionCreatorsNames.reduce((dispatchableActions, acName) => {
      const creator = actionCreators[acName];
      dispatchableActions[acName] = (...args: Parameters<typeof creator>) => {
        dispatch((creator as any)(...args));
      };
      return dispatchableActions;
    }, {} as DispatchableActionCreators<T>);
  };
}
