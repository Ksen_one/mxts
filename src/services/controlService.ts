import { EditorControlInfo } from "../components/Editor/controls/controlInfo";
import ControlType from "../components/Editor/controls/controlType";
import ControlBuilder from "../components/Editor/controls/controlBuilder";
import { ControlInfoDto } from "../components/Editor/controls/сontrolInfoDto";

export function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export function isRemovableControl(control: EditorControlInfo) {
  return control.type !== ControlType.Dashboard;
}

export function createEmptyDashboard(graph: mxGraph) {
  const dashboard = ControlBuilder.type(ControlType.Dashboard)
    .createControl()
    .createCell(graph);
  return dashboard;
}

export function restoreGraphControlTree(
  graph: mxGraph,
  controlInfoDto: ControlInfoDto,
  parent?: EditorControlInfo
) {
  const control = ControlBuilder.type(controlInfoDto.type)
    .createControl(parent)
    .setOptions(controlInfoDto.options)
    .createCell(graph, controlInfoDto.geometry);

  controlInfoDto.children.forEach(child =>
    restoreGraphControlTree(graph, child, control)
  );

  return control;
}
