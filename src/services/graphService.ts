import { EditorControlInfo } from "../components/Editor/controls/controlInfo";

export function resetPanningPosition(graph: mxGraph) {
  const padding = graph.gridSize * 5;
  graph.view.setTranslate(padding / 2, padding / 2);
}

export function isControlInfoCell(
  cell: mxCell
): cell is mxCell<EditorControlInfo> {
  return cell.value instanceof EditorControlInfo;
}

interface mxChildChange {
  child: mxCell<EditorControlInfo>;
  index?: number;
  model: mxGraphModel;
  parent: mxCell<EditorControlInfo> | null;
  previous: mxCell<EditorControlInfo> | null;
  previousIndex: number;
}

export function isMxChildChange(change: Object): change is mxChildChange {
  return change.constructor.name === "mxChildChange";
}

interface mxGeometryChange {
  cell: mxCell<EditorControlInfo>;
  geometry: mxGeometry;
  model: mxGraphModel;
  previous: mxGeometryChange;
}

export function isMxGeometryChange(change: Object): change is mxGeometryChange {
  return change.constructor.name === "mxGeometryChange";
}
