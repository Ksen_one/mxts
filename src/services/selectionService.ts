import { EditorControlInfo } from "../components/Editor/controls/controlInfo";
import { isRemovableControl } from "./controlService";

export function isRemovableSelection(selection: mxCell<EditorControlInfo>[]) {
  return selection.every(cell => cell.value && isRemovableControl(cell.value));
}
